(function (window, angular) {
	'use strict';
	/**
	 * @ngdoc directive
	 * @name directives:scroller
	 * @element scroller (E)
	 * @scope
	 * @description
	 * Wrapper for jquery scroller
	 * @param {array} scrollclass - Element Css Class name
	 * @example
	 * <scroller verticaltrackclass="@optional" verticalhandleclass="@optional"></scroller>
	*/
	angular.module('ph-scroller', []).
		directive('phScroller', ['$rootScope', function ($rootScope) {
			var directiveDefinitionObject = {
				priority: 0,
				restrict: 'A',
				scope: {
					verticaltrackclass: '@',
					verticalhandleclass: '@'
				},
				link: function postLink(scope, $scrollContainer) {
				    $rootScope.$on('$routeChangeStart', function (next, current) {
				        init();
				    });
				    setTimeout(function () {
				        init();
				    }, 0);

				    var $scrollContainerParent = $scrollContainer.parent();
				    var $verticaltrack = null;
				    var $verticalhandle = null;

				    function init() {
				        //;
				        initScrollBar();
				        initListener();
				    }

					function initScrollBar() {
						var verticaltrackclass = scope.verticaltrackclass || "phScroller-vertical-track",
							verticalhandleclass = scope.verticalhandleclass || "phScroller-vertical-handle"
						;

						//scroller with style. track3 and handle3 are styles connected to the main style  scrollboxBar
						$scrollContainer.enscroll({
							easingDuration: 300,
							showOnHover: false,
							pollChanges: true,
							addPaddingToPane: false,
							propagateWheelEvent: false,
							verticalTrackClass: verticaltrackclass,
							verticalHandleClass: verticalhandleclass
						});

						scope.$on('$destroy', function () {
							$scrollContainer.off('.phscroller');
						});

						$verticaltrack = $scrollContainerParent.find('.' + verticaltrackclass);
						$verticalhandle = $scrollContainerParent.find('.' + verticalhandleclass);

						$verticaltrack.mouseover(function () {
							displayScroll();
						});

						$verticaltrack.mouseout(function () {
							hideScroll();
						});

						$scrollContainer.mouseover(function () {
							displayScroll();
						});

						$scrollContainer.mouseout(function () {
							hideScroll();
						});

						function displayScroll() {
							$verticaltrack.addClass(verticaltrackclass + 'over');
							$verticalhandle.addClass(verticalhandleclass + 'over');
						};

						function hideScroll() {
							$verticaltrack.removeClass(verticaltrackclass + 'over');
							$verticalhandle.removeClass(verticalhandleclass + 'over');
						};
					}

					function initListener() {
						var scrollStart = true,
							timeout = null
						;

						$scrollContainer.off('.phscroller')
							.on('scroll.phscroller', function () {
							    //;
								if (scrollStart) {
									scrollStart = false;
									$rootScope.$broadcast('scrolling');
								}

								timeout && clearTimeout(timeout);
								timeout = setTimeout(function () {
									scrollStart = true;
									timeout = null;
								}, 150);
							});
					};
				}
			};
			return directiveDefinitionObject;
		}]);
})(window, window.angular);