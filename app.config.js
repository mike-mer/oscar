(function() {

	angular.module("oscar").constant('appConfig', new configs());



	function configs() {
		
		var remoteHost = localStorage.getItem('oscar.remote.host');
		
		this.version = "1.0.0.0";
		this.host = remoteHost || "";
	}

}());