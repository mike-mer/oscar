(function() {
	'use strict';

    var app = angular.module('oscar', [
        'ui.router',
        'ngSanitize',
        'ngRoute',
        'oc.lazyLoad',
        "ngMaterial",
        "ngAnimate",
        "ngAria",
        "ngMessages",
        "ph-scroller",
        "infiniteScroll",
        "ngResource",
        "ngMockE2E",
        'shagstrom.angular-split-pane',
        'ngDragDrop'
    ]);

    app.config(function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider, $ocLazyLoadProvider) {

        $ocLazyLoadProvider.config({
            'debug': true,
            'events': true
        });

        $locationProvider.html5Mode(false).hashPrefix('');
        $urlRouterProvider.otherwise(function() {window.location.href = 'login.html';});

        $stateProvider
            .state("dashboard", {
                url: "/dashboard/{id}",
                template: "<profile></profile>",
                params: {
                    id: { squash: true, value: '' }
                },
                resolve: {
                    load: ['$ocLazyLoad', function($ocLazyLoad) {
                        $(".page-loading-overlay").removeClass("loaded");
                        return $ocLazyLoad.load(['components/profile/profile.js'])
                                .then(function() { $(".page-loading-overlay").addClass("loaded"); });
                    }]
                }
            })
            .state("es_gallery", {
                url: "/es/gallery",
                template: "<es-gallery></es-gallery>",
                resolve: {
                    load: ['$ocLazyLoad', function($ocLazyLoad) {
                        $(".page-loading-overlay").removeClass("loaded");
                        return $ocLazyLoad.load(['components/es/es-gallery.js'])
                                .then(function() { $(".page-loading-overlay").addClass("loaded"); });
                    }]
                }
            })
            .state("cifer", {
                url: "/cifer",
                template: "<cifer></cifer>",
                resolve: {
                    load: ['$ocLazyLoad', function($ocLazyLoad) {
                        $(".page-loading-overlay").removeClass("loaded");
                        return $ocLazyLoad.load(['components/cifer/cifer.js'])
                                .then(function() { $(".page-loading-overlay").addClass("loaded"); });
                    }]
                }
            })
            .state("ed_new", {
                url: "/ed/new",
                template: "<ed-new></ed-new>",
                resolve: {
                    load: ['$ocLazyLoad', function($ocLazyLoad) {
                        $(".page-loading-overlay").removeClass("loaded");
                        return $ocLazyLoad.load(['components/ed/ed-new.js'])
                                .then(function() { $(".page-loading-overlay").addClass("loaded"); });
                    }]
                }
            })
            .state("ed_gallery", {
                url: "/ed/gallery",
                template: "<ed-gallery></ed-gallery>",
                resolve: {
                    load: ['$ocLazyLoad', function($ocLazyLoad) {
                        $(".page-loading-overlay").removeClass("loaded");
                        return $ocLazyLoad.load(['components/ed/ed-gallery.js'])
                                .then(function() { $(".page-loading-overlay").addClass("loaded"); });
                    }]
                }
            })
            .state("nsd_gallery", {
                url: "/nsd/gallery",
                template: "<nsd-gallery></nsd-gallery>",
                resolve: {
                    load: ['$ocLazyLoad', function($ocLazyLoad) {
                        $(".page-loading-overlay").removeClass("loaded");
                        return $ocLazyLoad.load(['components/nsd/nsd-gallery.js'])
                                .then(function() { $(".page-loading-overlay").addClass("loaded"); });
                    }]
                }
            })
            .state("nsd-results", {
                url: "/nsd/results/{id}",
                template: "<nsd-results></nsd-results>",
                params: {
                        id: {squash: true, value: ''}
                },
                resolve: {
                    load: ['$ocLazyLoad', function($ocLazyLoad) {
                        $(".page-loading-overlay").removeClass("loaded");
                        return $ocLazyLoad.load(['components/nsd/nsd-results.js'])
                                .then(function() { $(".page-loading-overlay").addClass("loaded"); });

                    }]
                }
            }).state("profile", {
                url: "/profile/{id}",
                template: "<dashboard></dashboard>",
                params: {
                    id: { squash: true, value: '' }
                },
                resolve: {
                    load: ['$ocLazyLoad', function($ocLazyLoad) {
                        $(".page-loading-overlay").removeClass("loaded");
                        return $ocLazyLoad.load(['components/dashboard/dashboard.js'])
                                .then(function() { $(".page-loading-overlay").addClass("loaded"); });
                    }]
                }
            })
             .state('nsd_gallery.popup', {
                parent: 'nsd_gallery',
                url: "/popup",
                 views: {
                     'popupContent': {
                         template: '<nsd-new></nsd-new>',
                         resolve: {
                             load: ['$ocLazyLoad', function($ocLazyLoad) {
                                 $(".page-loading-overlay").removeClass("loaded");
                                 return $ocLazyLoad.load(['controls/dictionaryTree/dictionaryTree.js', 'components/nsd/nsd-new.js'])
                                    .then(function() { $(".page-loading-overlay").addClass("loaded"); });
                             }]
                         }
                     }
                 }
             })
            .state("nsd_new", {
                url: "/nsd/new/{id}",
                template: "<nsd-new></nsd-new>",
                params: {
                        id: {squash: true, value: ''}
                },
                resolve: {
                    load: ['$ocLazyLoad', function($ocLazyLoad) {
                        $(".page-loading-overlay").removeClass("loaded");
                        return $ocLazyLoad.load(['controls/dictionaryTree/dictionaryTree.js', 'components/nsd/nsd-new.js'])
                                .then(function() { $(".page-loading-overlay").addClass("loaded"); });
                    }]
                }
            })
            .state("ed_results", {
                url: "/ed/results/{id}",
                template: "<ed-results></ed-results>",
                params: {
                    id: {squash: true, value: ''}
                },
                resolve: {
                    load: ['$ocLazyLoad', function($ocLazyLoad) {
                        $(".page-loading-overlay").removeClass("loaded");
                        return $ocLazyLoad.load(['components/ed/ed-results.js'])
                                .then(function() { $(".page-loading-overlay").addClass("loaded"); });
                    }]
                }
		    });
    });

    angular.module('oscar').run(function($rootScope, $httpBackend) {
         $httpBackend.whenGET(/.*/).passThrough();
         $httpBackend.whenPOST(/.*/).passThrough();
         $httpBackend.whenJSONP(/.*/).passThrough();
         $httpBackend.whenPUT(/.*/).passThrough();
    });

    angular.element(document.body).ready(function() {
        angular.bootstrap(document, ['oscar']);
    });

}());