(function () {
	'use strict';
	angular.module("oscar").component('mapPannel', {
		bindings: {
			profile: '<'
		},
		templateUrl: 'components/pannels/mapPannel/mapPannel.html',
		controller: mapPannelController
	}).service('mapPannelModel', mapPannelModel);

	mapPannelController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'mapPannelModel', 'api', '$compile', '$scope', '$state', '$ocLazyLoad'];
	function mapPannelController(generalService, api, popups, appConfig, mapPannelModel, apiService, $compile, $scope, $state, $ocLazyLoad) {
		var self = this;
		this.baseImageUrl = appConfig.host;
		this.$onChanges = function (changesObj) {
			if(changesObj.profile && changesObj.profile.currentValue != undefined) {
                jsLoader.loadScript("https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDC0OfHIpP5CsdMkzORtvF-6YrVjThG6Gc", function() {
                    init();
                });
			}
		};

		function init() {
			setTimeout(function () {
                var geocoder = new google.maps.Geocoder();
                var mapOptions = {
					  zoom: 4,
					  center: {lat: 31, lng: 35},
					  mapTypeControl: true
                };
				if (self.profile.checkins && self.profile.checkins.length) {
					mapOptions.center = new google.maps.LatLng(self.profile.checkins[0].latitude, self.profile.checkins[0].longtitude)
				}

				function centerMarkers(markers, map) {
					var bounds = new google.maps.LatLngBounds();
					for(i=0; i<markers.length; i++) {
					   bounds.extend(markers[i].getPosition());
					}

					map.setCenter(bounds.getCenter());

					map.fitBounds(bounds);

					var newZoom = map.getZoom()-1;
					map.setZoom(newZoom || 1);

					if(map.getZoom()> 15){
					  map.setZoom(15);
					}
				}

                var mapEl = document.getElementById('map-1');
                mapEl.style.height = '100%';
                var map = new google.maps.Map(mapEl, mapOptions);

				var markers = [];
				var info_window = new google.maps.InfoWindow({content: ''});
				
				for (var i in self.profile.placesLived) {
                    var checkin = self.profile.placesLived[i];
					if (checkin.placeName) {
						geocoder.geocode( { 'address': checkin.placeName}, function(results, status) {
							var marker = new google.maps.Marker({
								map: map,
								position: results[0].geometry.location,
								title: results[0].formatted_address,
								icon: "styles/images/place.png"
							});
								marker.note = results[0].formatted_address;
								google.maps.event.addListener(marker, 'click', function() {
									info_window.setContent("<span style='font-size:20px; display:inline-block; color:black;'>" + this.note + "</span>");
									info_window.open(map, this);
								});
							markers.push(marker);
							centerMarkers(markers, map);
						});
					}
				}

                for (var i in self.profile.checkins) {
                    var checkin = self.profile.checkins[i];
                    if (checkin.description) {
                    	geocoder.geocode( { 'address': checkin.description}, function(results, status) {
                    		 var marker = new google.maps.Marker({
                                 map: map,
                                 position: results[0].geometry.location,
         						 title: results[0].formatted_address,
         						 icon: "styles/images/checkin.png"
                             });
	                    	marker.note = results[0].formatted_address;
	     					google.maps.event.addListener(marker, 'click', function() {
	     						info_window.setContent("<span style='font-size:20px; display:inline-block; color:black;'>" + this.note + "</span>");
	     						info_window.open(map, this);
	     					});
     					markers.push(marker);
     					centerMarkers(markers, map)
                    	});		
                    }
                }

				for (var i in self.profile.citiesVisited) {
                    var checkin = self.profile.citiesVisited[i];
					if (checkin.placeName) {
						geocoder.geocode( { 'address': checkin.placeName}, function(results, status) {
							var marker = new google.maps.Marker({
								map: map,
								position: results[0].geometry.location,
								title: results[0].formatted_address,
								icon: "styles/images/city.png"
							});
								marker.note = results[0].formatted_address;
								google.maps.event.addListener(marker, 'click', function() {
									info_window.setContent("<span style='font-size:20px; display:inline-block; color:black;'>" + this.note + "</span>");
									info_window.open(map, this);
								});
							markers.push(marker);
							centerMarkers(markers, map)
						});
					}
				}
            }, 200);
		}
	}

	function mapPannelModel() {
		  this.data = {};
	}
})();
