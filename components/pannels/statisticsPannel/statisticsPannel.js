(function () {
	'use strict';
	angular.module("oscar").component('statisticsPannel', {
		bindings: {
			categories: '<',
			updateCategories: '&',
      updateSelectedCategoriesCallback: '&?'
		},
		templateUrl: 'components/pannels/statisticsPannel/statisticsPannel.html',
		controller: statisticsPannelController
	}).service('statisticsPannelModel', statisticsPannelModel);

	statisticsPannelController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'statisticsPannelModel', 'api', '$compile', '$scope', '$state', '$ocLazyLoad', '$element'];
	function statisticsPannelController(generalService, api, popups, appConfig, statisticsPannelModel, apiService, $compile, $scope, $state, $ocLazyLoad, $element) {
		var self = this;
		this.baseImageUrl = appConfig.host;
		this.sourceId = $state.params.id;
		this.xenonPalette = ['#68b828', '#7c38bc', '#0e62c7', '#fcd036', '#c0392b', '#00b19d', '#ff6264', '#f7aa47'];

        this.updateSelectedCategories = function(categories) {
            categories.forEach(function(c) {
                self.categories.filter(function(cat) {return cat.name == c.name}).map(function(cat) { cat.isSelected = c.isSelected});
            })
        };
    
		this.$onInit = function () {
            if(this.updateSelectedCategoriesCallback) {
                this.updateSelectedCategoriesCallback( {subscriber : self.updateSelectedCategories});
            }
		};

		this.$onChanges = function (changesObj) {
			if(changesObj.categories && changesObj.categories.currentValue != undefined) {
				init();
			}
		};

		this.runFullLA = function () {
            var profileApiData = { module: "profile", method: "runFullLA", data: { id: self.sourceId } };
            apiService.execute(profileApiData).then(function (response) {
            });
        };

		this.dictFilterChanged = function () {
			self.updateCategories({categories: self.categories});
			return;
        };

		function init() {
			self.categories = angular.copy(self.categories);
			var paletteIndex = -1;
			$element.find(".catChart").empty().dxChart({
				dataSource: self.categories,
				barWidth: 0.5,
				rotated: true,
				 legend: {
					visible: false
				},
				argumentAxis: {
						label: {
							customizeText: function(e) {
								return "";
							}
						}
				},
				series: {
					argumentField: "categoryName",
					valueField: "usersCount",
					type: "bar"
				}, customizePoint: function () {
					paletteIndex++;
					if (self.xenonPalette.length === paletteIndex)
						paletteIndex = 0;
					var color = self.xenonPalette[paletteIndex];

					return {
						color: color
					};
				},
				tooltip: {
					enabled: true,
					location: "edge",
					customizeTooltip: function (arg) {
						return {
							text: arg.argumentText + " - " + arg.valueText
						};
					}
				}
			});
		}
	}
	function statisticsPannelModel() {
		  this.data = {};
	}
})();
