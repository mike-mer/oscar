(function () {
    'use strict';

    angular.module("oscar").component('cloudPannel', {
        bindings: {
            profile: '<'
        },
        templateUrl: 'components/pannels/cloudPannel/cloudPannel.html',
        controller: cloudPannelController
    }).service('cloudPannelModel', cloudPannelModel);

    cloudPannelController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'cloudPannelModel', 'api', '$compile', '$scope', '$state', '$ocLazyLoad'];
    function cloudPannelController(generalService, api, popups, appConfig, cloudPannelModel, apiService, $compile, $scope, $state, $ocLazyLoad) {
        var self = this;
        this.baseImageUrl = appConfig.host;

        this.$onChanges = function (changesObj) {
            if (changesObj.profile && changesObj.profile.currentValue != undefined) {
                init();
            }
        };

        function init() {
            var wordsDataSource = self.profile.dominantFamilies.map(function (df) {
                return {text: df.familyName, weight: df.familySize};
            });
            $('#TagCloud').jQCloud(wordsDataSource, {delay: 50});
        }
    }

    function cloudPannelModel() {
        this.data = {};
    }
})();
