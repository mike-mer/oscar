(function () {
	'use strict';
	angular.module("oscar").component('rangeChartPannel', {
		bindings: {
			profile: '<'
		},
		templateUrl: 'components/pannels/rangeChartPannel/rangeChartPannel.html',
		controller: rangeChartPannelController
	}).service('rangeChartPannelModel', rangeChartPannelModel);

	rangeChartPannelController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'rangeChartPannelModel', 'api', '$compile', '$scope', '$state', '$ocLazyLoad', '$element'];
	function rangeChartPannelController(generalService, api, popups, appConfig, rangeChartPannelModel, apiService, $compile, $scope, $state, $ocLazyLoad, $element) {   
		var self = this;
		this.baseImageUrl = appConfig.host;
		this.sourceId = $state.params.id;

		this.$onChanges = function (changesObj) {
			if(changesObj.profile && changesObj.profile.currentValue != undefined) {
				init();
			}
		};

		function init() {
			apiService.execute({ module: "profile", method: "getAnalysisPerTimeResults", data: { id: self.sourceId } }).then(function (response) {
                var colors = ['#68b828', '#7c38bc', '#40bbea'];
                var idx = -1;
                var series = [];
                var all_data_sources = [];
                var newPropNameDict = {};
                for (var i in response.data.data) {
                    var itm = response.data.data[i];
                    for (var propName in itm) {
                        if (propName === 'period' || !itm.period) {
                            continue;
                        }
                        for (var innerPropName in itm[propName]) {
                            var parseDate = itm.period.split("-");
                            var newItm = { time: new Date(parseDate[0], parseDate[1], 1), };
                            var newPropName = (propName + "_" + innerPropName).toLowerCase().replace(' ', '');
                            newItm[newPropName] = itm[propName][innerPropName];
                            all_data_sources.push(newItm);
                            if (!newPropNameDict[newPropName]) {
                                series.push({ argumentField: "time", valueField: newPropName, color: colors[++idx], opacity: .65 });
                                newPropNameDict[newPropName] = true;
                            }
                        }
                    }
                }
                $element.find(".range-chart").dxRangeSelector({
                    dataSource: all_data_sources,
                    size: {
                        height: 140
                    },
                    chart: {
                        series: series
                    },

                    selectedRangeChanged: function (e) {
                        var filter = {};
                        for (var i in all_data_sources[0]) {
                            filter[i] = [];
                        }

                        $.map(all_data_sources, function (arg, i) {
                            if (date("U", e.startValue) <= date("U", arg.time) && date("U", e.endValue) >= date("U", arg.time)) {
                                for (var i in all_data_sources[0]) {
                                    filter[all_data_sources[0][i]].push({
                                        time: arg.time,
                                        reqs: arg[all_data_sources[0]]
                                    });
                                }
                            }
                        });

                        for (var i in all_data_sources[0]) {
                            $('#' + all_data_sources[0][i]).dxChart('instance').option('dataSource', filter[all_data_sources[0][i]]);
                        }
                    }
                });
                $element.find(".range-chart").data("dxRangeSelector").render();
			});
		}
	}
	function rangeChartPannelModel() {
		  this.data = {};
	}
})();