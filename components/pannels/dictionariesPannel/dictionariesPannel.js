(function () {
  'use strict';

	angular.module("oscar").component('dictionariesPannel', {
		bindings: {
			categories: '<'
		},
		templateUrl: 'components/pannels/dictionariesPannel/dictionariesPannel.html',
		controller: dictionariesPannelController
	}).service('dictionariesPannelModel', dictionariesPannelModel);

	dictionariesPannelController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'dictionariesPannelModel', 'api', '$compile', '$scope', '$state', '$ocLazyLoad', '$element', '$timeout'];
	function dictionariesPannelController(generalService, api, popups, appConfig, dictionariesPannelModel, apiService, $compile, $scope, $state, $ocLazyLoad, $element, $timeout) {   
		var self = this;
		this.baseImageUrl = appConfig.host;
		this.sourceId = $state.params.id;

		this.$onChanges = function (changesObj) {
			if(changesObj.categories && changesObj.categories.currentValue != undefined) {
				init();
			}
		};

		function init() {
			$element.find(".barGauge").empty().append(
				'<div class="gauge" style="width: 100%; height: 50%; margin-top: 0px; "></div>',
				'<div id="panel" style="width: 100%; text-align: left; margin-top:0px;"></div>'
			);

			$timeout(function() {
				initGauge();
			})
		}

		function initGauge() {
			var xenonPalette = ['#68b828', '#7c38bc', '#0e62c7', '#fcd036', '#c0392b', '#00b19d', '#ff6264', '#f7aa47'];

			self.categories.forEach(function(item) {
				item.name = item.categoryName;
				item.count = item.categoryPercentage;
			});
			
			var gauge = $element.find('.gauge').dxBarGauge({
				startValue: 0,
				endValue: 100,
				label: {
					format: 'fixedPoint',
					precision: 0
				},
				tooltip: {
					enabled: true,
					customizeTooltip: function (arg) {
						return {
							text: self.categories[arg.index].name + " - " + arg.valueText + "%"
						};
					}
				},
				palette: xenonPalette
			}).dxBarGauge('instance');

			var values = $.map(self.categories, function (input) {
				return input.count;
			});
			gauge.values(values);
		}
	}
	
	function dictionariesPannelModel() {
		  this.data = {};
	}
})();
