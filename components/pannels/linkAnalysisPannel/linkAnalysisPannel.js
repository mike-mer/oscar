(function () {
  'use strict';
	angular.module("oscar").component('linkAnalysisPannel', {
		bindings: {
			profile: '<',
			updateGraphData: '&',
			updateSelected: '&'
		},
		templateUrl: 'components/pannels/linkAnalysisPannel/linkAnalysisPannel.html',
		controller: linkAnalysisPannelController
	}).service('linkAnalysisPannelModel', linkAnalysisPannelModel);

	linkAnalysisPannelController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'linkAnalysisPannelModel', 'api', '$compile', '$scope', '$state', '$ocLazyLoad'];
	function linkAnalysisPannelController(generalService, api, popups, appConfig, linkAnalysisPannelModel, apiService, $compile, $scope, $state, $ocLazyLoad) {
		var self = this;
        this.xenonPalette = ['#68b828', '#7c38bc', '#0e62c7', '#fcd036', '#c0392b', '#00b19d', '#ff6264', '#f7aa47'];
		this.sourceId = $state.params.id;
		this.baseImageUrl = appConfig.host;

		this.$onChanges = function (changesObj) {
			if(changesObj.profile && changesObj.profile.currentValue != undefined) {
				init();
			}
		};

		function nodeClickFn(event, args, node) {
            var selectedNode = args.selection.length ?  args.selection[0].data : node;
            console.log(selectedNode);
            self.updateSelected({info : (self.sourceId == args.selection[0].data.id ? null :selectedNode)});
        }

        function setGraphData(graphData) {
            self.graphControl = SocialSetGraph(self.graphData, self.sourceId, nodeClickFn);
			self.updateGraphData({graphData: graphData, graphControl: self.graphControl});
        }

		function init() {
            self.graphData = { nodes: [{ id: self.sourceId, extra: { name: self.profile.fullName || "source", image:  (self.profile.mainProfilePicture ? self.baseImageUrl + "/EntitiesDiscovery/images/" + self.profile.mainProfilePicture.imageId : "") }, }] };
            self.graphData.nodes = self.graphData.nodes.concat(self.profile.friends.map(function (f) {
				f = angular.copy(f);
				var target = f.target;
				var catagoriesPercentages = f.catagoriesPercentages;
				delete f.target;
				delete f.catagoriesPercentages;
				var counters = f;
				f = {};
				f.counters = counters;
				f.catagoriesPercentages = catagoriesPercentages;
				f.target = target;
                return { id: f.target.userId.toString(), extra: { networkName: f.target.networkName, name: f.target.fullName, image: self.baseImageUrl + "/EntitiesDiscovery/images/" + f.target.image.imageId, counters: f.counters, categories: f.catagoriesPercentages }, };
            }));

            self.graphData.orgNodes = self.graphData.nodes;

            var counter = 0;
            self.graphData.links = self.profile.friends.map(function (l) {
                return { id: counter++, from: self.sourceId.toString(), to: l.target.userId.toString(), extra: { type: l.target.networkName }, };
            });

            setGraphData(self.graphData);
		}
	}

	function linkAnalysisPannelModel() {
		  this.data = {};
	}
})();
