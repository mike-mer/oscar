(function () {
  'use strict';

	angular.module("oscar").component('detailsPannel', {
		bindings: {
			profile: '<'
		},
		templateUrl: 'components/pannels/detailsPannel/detailsPannel.html',
		controller: detailsPannelController
	}).service('detailsPannelModel', detailsPannelModel);

	detailsPannelController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'detailsPannelModel', 'api', '$compile', '$scope', '$state', '$ocLazyLoad'];
	function detailsPannelController(generalService, api, popups, appConfig, detailsPannelModel, apiService, $compile, $scope, $state, $ocLazyLoad) {
		this.baseImageUrl = appConfig.host;
	}

	function detailsPannelModel() {
		  this.data = {};
	}
})();
