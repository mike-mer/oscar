(function () {
	'use strict';
	angular.module("oscar").component('timelinePannel', {
		bindings: {
			profile: '<',
			categories: '<'
		},
		templateUrl: 'components/pannels/timelinePannel/timelinePannel.html',
		controller: timelinePannelController
	}).service('timelinePannelModel', timelinePannelModel);

	timelinePannelController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'timelinePannelModel', 'api', '$compile', '$scope', '$state', '$ocLazyLoad', '$element'];
	function timelinePannelController(generalService, api, popups, appConfig, timelinePannelModel, apiService, $compile, $scope, $state, $ocLazyLoad, $element) {
		var self = this;
		this.baseImageUrl = appConfig.host;
		this.sourceId = $state.params.id;

		this.$onChanges = function (changesObj) {
			if(changesObj.profile && changesObj.profile.currentValue != undefined) {
				init();
			}
			if(changesObj.categories && changesObj.categories.currentValue != undefined) {
				self.categoriesAll = angular.copy(self.categories);
			}
		};

		function createGuid() {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
				var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			});
		}

		function getSourceStyleByName(name) {
			switch (name) {
				case "facebook" :
					return { icon: "socicon-facebook", background : "#3e5b98", url : "https://www.facebook.com/" };
				case "twitter" :
					return { icon: "socicon-twitter", background : "#4da7de", url : "https://twitter.com/" };
				case "googleplus" :
					return { icon: "socicon-googleplus", background : "#dd4b39" };
				case "linkedin" :
					return { icon: "socicon-linkedin", background : "#3371b7" };
				case "windows" :
					return { icon: "fa-windows", background : "#dd4b39" };
				case "bing" :
					return { icon: "custom-bing", background : "transparent" };
				case "google" :
					return { icon: "custom-google", background : "transparent" };
				case "sixGil" :
					return { icon: "socicon-envato", background : "#800080" };
				default :
					return { icon: "", background : "#3371b7" };
			}
		}

        function init() {
			apiService.execute({ module: "profile", method: "getEntityStatisticsContent", data: { id: self.sourceId } }).then(function (response) {
				response.data.timelineItems = response.data.timelineItems || [];
				self.posts = response.data.timelineItems;

				self.posts.forEach(function(p) {
					p.style = getSourceStyleByName(p.networkName);
				});

				var expl = {};
				for (var i in self.posts) {
					var tl = self.posts[i];

					var postMarks = [];
					if (!tl.categories)
						continue;

					for (var catIdx in tl.categories) {
						var cat = tl.categories[catIdx];

						if (!expl[cat.categoryName]) {
							expl[cat.categoryName] = { count: 0, words: {}};
						}
						for (var catWordIdx in cat.categoryWords) {
							var catWord = cat.categoryWords[catWordIdx];

							if (!expl[cat.categoryName].words[catWord.word]) {
								expl[cat.categoryName].count++;
								expl[cat.categoryName].words[catWord.word] = {categoryScore : cat.categoryScore, score: catWord.score, translation: catWord.translation || catWord.word, explanation : catWord.explanation, count: 0, instances: [], wordInstanceId : createGuid()  };
							}

							for (var wordStartIdx in catWord.startIndexes) {
								var postMark = angular.copy(catWord);
								postMark.startIndexes = null;
								postMark.startIdx = catWord.startIndexes[wordStartIdx];
								postMark.id = createGuid();
								postMark.categoryName = cat.categoryName;

								expl[cat.categoryName].words[catWord.word].style = function () {
																		//debugger;
									return {
										'background-color': this.highlighted ? (this.score ? ( this.score < 5 ? '#D0D04B' : (this.score < 8 ? "orange" : "#FFA6A1")) : "#aaa") : 'rgba(202,230,244,0.4)'
									};
								};
								expl[cat.categoryName].words[catWord.word].expStyle = function () {
									return {
										'background-color': this.highlighted ? (this.score ? ( this.score < 5 ? '#D0D04B' : (this.score < 8 ? "orange" : "#FFA6A1")) : "#aaa") : 'transparent'
									};
								};

								expl[cat.categoryName].words[catWord.word].instances.push(postMark.id);
								postMarks.push(postMark);
							}
							expl[cat.categoryName].words[catWord.word].count += catWord.startIndexes.length;
						}
					}

					self.postMarks = postMarks;
					var postParts = [];
					var lastIdx = 0;
                    if(tl.content && tl.content.length) {
                        tl.content = tl.content.replace(/\s+/g," ");
                    }

					postMarks = postMarks.sort(function (a, b) { return a.startIdx - b.startIdx; });
					for (var markIdx in postMarks) {
						if (lastIdx < postMarks[markIdx].startIdx) {
                            postParts.push(tl.content.substring(lastIdx, postMarks[markIdx].startIdx));
                        }

						var color = (postMarks[markIdx].score <= 5 ? '#D0D04B' : (postMarks[markIdx].score <= 8 ? 'orange' : 'red'));
						postParts.push("<span style='cursor:pointer' ng-click='$ctrl.wordClick(\"" + postMarks[markIdx].categoryName + "\", \"" + postMarks[markIdx].word + "\")' mark-id='" + postMarks[markIdx].id + "' ng-style='$ctrl.currentExplanations[\"" + postMarks[markIdx].categoryName + "\"].words[\"" + postMarks[markIdx].word + "\"].style()'>" + tl.content.substr(postMarks[markIdx].startIdx, postMarks[markIdx].word.length) + "</span>");

						lastIdx = postMarks[markIdx].startIdx + postMarks[markIdx].word.length;
					}
					postParts.push(tl.content.substr(lastIdx));

					tl.content = postParts.join("");
				}
				self.currentExplanations = expl;
			});
		}

        this.closeOtherGroups = function(current) {
            for (var grp in self.currentExplanations) {
                var group = self.currentExplanations[grp];
                if (current != group) {
                    group.open = false;
                }
            }
        };

		self.toDate = function (num) {
            return new Date(num);
        };

		self.xenonPalette = ['#68b828', '#7c38bc', '#0e62c7', '#fcd036', '#c0392b', '#00b19d', '#ff6264', '#f7aa47'];

	    var groupBy = function (xs, key) {
            return xs.reduce(function (rv, x) {
                (rv[x[key]] = rv[x[key]] || []).push(x);
                return rv;
            }, {});
        };

        self.guid = function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        };

        self.marksStore = {};
        self.marksMap = {};
        self.wordsMap = {};

        self.countOpenedGroups = function () {
            var currentExplanations = self.currentExplanations;
            var currentExplanationsArr = Object.keys(currentExplanations).map(function (key) { return currentExplanations[key] });
            var n = currentExplanationsArr.filter(function (g) { return g.open }).length;
            var percent = !n ? "30px" : ((100 / n) + "%");
            return percent;
        };

        self.markClick = function (contentGuid, guid) {
            var store = self.marksMap;
            self.fullMode = true;
            if (contentGuid) {
                self.fullMode = false;
                if (!self.fullMode) {
                    if (self.lastMarked && self.lastMarked != store[contentGuid][guid]) {
                        self.lastMarked.highlighted = false;
                    }
                    if (self.lastMarked) {
                        self.wordsMap[self.lastMarked.category][self.lastMarked.word].highlighted = false;
                    }
                    store[contentGuid][guid].highlighted = !store[contentGuid][guid].highlighted;
                    self.lastMarked = store[contentGuid][guid].highlighted ? store[contentGuid][guid] : null;
                } else {
                    if (self.lastMarked && self.lastMarked.category != store[contentGuid][guid].category && self.lastMarked.word != store[contentGuid][guid].word) {
                        self.wordsMap[self.lastMarked.category][self.lastMarked.word].highlighted = false;
                    }
                    self.wordsMap[store[contentGuid][guid].category][store[contentGuid][guid].word].highlighted = !self.wordsMap[store[contentGuid][guid].category][store[contentGuid][guid].word].highlighted;
                    self.lastMarked = self.wordsMap[store[contentGuid][guid].category][store[contentGuid][guid].word].highlighted ? store[contentGuid][guid] : null;
                }

            } else {
                if (self.lastMarked) {
                    self.lastMarked.highlighted = false;
                    self.lastMarked = null;
                }
            }

            if (self.lastMarked == null && contentGuid) {
                contentGuid = null;
                self.fullMode = true;
            }

            var marks, categoriesArr;

            if (contentGuid) {
                marks = store[contentGuid];
                categoriesArr = Object.keys(marks).map(function (key) { return marks[key] });
            } else {
                var cotegoriesArrJoin = [];
                for (var catN in store) {
                    var ctgr = store[catN];
                    var ctgrArr = Object.keys(ctgr).map(function (key) { return ctgr[key] });
                    ctgrArr = ctgrArr.filter(function (item) { return cotegoriesArrJoin.filter(function (i) { return i.category == item.category && i.word == item.word }).length == 0 });
                    cotegoriesArrJoin = cotegoriesArrJoin.concat(ctgrArr)
                }
                categoriesArr = cotegoriesArrJoin;
            }

            var categories = groupBy(categoriesArr, 'category');
            var categoriesUI = {};
            for (var cat in categories) {
                var category = categories[cat];
                categoriesUI[cat] = { items: category };
                var isActiveGroup = category.filter(function (c) { return c.highlighted || c.category == self.dictionary }).length > 0;
                categoriesUI[cat].open = isActiveGroup;
            }
            self.currentExplanations = categoriesUI;
        };

		function openGategory(c, w, m) {
  			var expls = self.currentExplanations;
  			for (var e in expls) {
  				var exp = expls[e];
  				exp.open = (c == e);
  			}

            var element = $(".word-id-" + m.wordInstanceId);

            var parent = $("#explanationsContainer");
            var elementTop = element.offset().top;
            parent.animate({ 'scrollTop': (elementTop + parent[0].scrollTop - parent.offset().top - 30) + 'px' }, '500');
		}

		this.wordClick = function(category, word) {
			var mark = self.currentExplanations[category].words[word];
			if (self.lastMark) {
				if (mark == self.lastMark) {
					self.lastMark.highlighted = false;
					self.lastMark = null;
				} else {
					self.lastMark.highlighted = false;
					self.lastMark = mark;
					self.lastMark.highlighted = true;
					!self.FilterMode && openGategory(category, word, mark);
				}
			} else {
					self.lastMark = mark;
					self.lastMark.highlighted = true;
					!self.FilterMode && openGategory(category, word, mark);
			}
		};

		this.lastMark = null;
        self.nextInstance = function (word, w, g, k) {

			if (!self.lastMark) {
				self.lastMark = self.currentExplanations[k].words[w];
				self.lastMark.highlighted = true;
			} else {
				if (self.lastMark == self.currentExplanations[k].words[w]) {

				} else {
					self.lastMark.highlighted = false;
					self.lastMark = self.currentExplanations[k].words[w];
					self.lastMark.highlighted = true;
				}
			}

            if (!word.currPointer && word.currPointer !== 0)
                word.currPointer = -1;

            word.currPointer++;

            if (word.currPointer == word.count)
                word.currPointer = 0;

            var area = $(".cbp_tmtimeline");
            var nextMarkId = word.instances[word.currPointer];
            var match = $("[mark-id='" + nextMarkId + "']");
            var matchTop = match.offset().top;

            console.log(matchTop, area[0].scrollTop, area.offset().top, match.scrollTop(), match.position().top, matchTop + area[0].scrollTop, matchTop - area[0].scrollTop);
            area.animate({ 'scrollTop': (matchTop + area[0].scrollTop - area.offset().top - 30) + 'px' }, '500');

            var animationInterval = setInterval(function () {
                if (match.css('opacity') == 1) {
                    match.animate({ 'opacity': '0' }, 220);
                } else {
                    match.animate({ 'opacity': '1' }, 220);
                }
            }, 230);

            setTimeout(function () {
                clearInterval(animationInterval);
                match.animate({ 'opacity': '1' }, 100);
            }, 1300);
        };

        self.runFullLA = function () {
            var profileApiData = { module: "profile", method: "runFullLA", data: { id: self.sourceId } };
            apiService.execute(profileApiData).then(function (response) {
            });
        };

		self.dictFilterPostsChanged = function(category) {
  			self.categoriesAll.forEach(function(c) {
  				    c.isSelected = (c.categoryName == category.categoryName ? true : false);
  			});

            for (var i in self.posts) {
                      var toShow = (category.categoryName == "All") || (self.posts[i].categories && ( self.posts[i].categories && self.posts[i].categories.some(function (c) { return category.categoryName == c.categoryName }) ) )
                  self.posts[i].hidden = !toShow;
            }

            var expls = self.currentExplanations;
            for (var e in expls) {
                var exp = expls[e];
                exp.hidden = category.categoryName == "All" ? false : (e != category.categoryName);
                exp.open = category.categoryName == "All" ? false : (e == category.categoryName);
            }
            self.FilterMode = (category.categoryName != "All");
		};
	}

	function timelinePannelModel() {
		  this.data = {};
	}
})();