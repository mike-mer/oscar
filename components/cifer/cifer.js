(function () {
  'use strict';
  
	angular.module("oscar").component('cifer', {
		bindings: {},
		templateUrl: 'components/cifer/cifer.html',
		controller: ciferController
	}).service('ciferModel', ciferModel);
	
	
	ciferController.$inject = ['ciferModel', 'api', '$compile', '$scope'];
	function ciferController(ciferModel, apiService, $compile, $scope) {
		var self = this;

		this.$onInit = function () {
			var apiData = { module: "NSD", method: "getDictionaries", data: {}};
			apiService.execute(apiData)
			.then(
				function(responce) {
					self.dictionaries = responce.map(function(d) { return {name:d} });
				}
			);
		};
		
		
		this.analyze = function() {
			var text = JSON.stringify(self.text);
			var apiData = { module: "CIFER", method: "test", data: { text: text, dictionary : self.selectedDictionary } };
			apiService.execute(apiData).then(function(response) {
				self.results = response;
				self.init();
			});
		};

		this.editMode = true;
				
		this.guid = function() {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
			return v.toString(16);
			});
		};
		
		this.marksMap = {};
	
		this.next = function(obj, event) {
			if (event) {
				event.stopPropagation();
			}

			if (!obj.highlighted) {
				this.explanationClick(obj);
				return;
			}

			var category = obj.category;
			var word = obj.word;
			var tab = 0;
			if (this.lastFlash && this.lastFlash.category == category && this.lastFlash.word == word) {
				tab = this.lastFlash.tab + 1;
			}

			var matches = $("#previewSpan span[data-category='" + category + "'][data-word='" + word + "']");
			if (tab >= matches.length) {
				tab = 0;
			}

			this.lastFlash = {category : category, word: word, tab : tab};

			var match = matches.eq(tab);
			var top = match.offset().top;

			var area = $("#previewSpanArea");
			var content = $("#previewSpan").eq(0);
			var contentTop = content.offset().top;

			area.animate({ 'scrollTop': (top - contentTop) + 'px' }, '500');


			///////////////////////////////////////////////////////// blink animation ///////////////////////////////////////////////////////////
			var animationInterval = setInterval(function () {
				if (match.css('opacity') == 1) {
					match.animate({ 'opacity': '0' }, 220);
				} else {
					match.animate({ 'opacity': '1' }, 220);
				}
			}, 230);

			setTimeout(function () {
				clearInterval(animationInterval);
				match.animate({ 'opacity': '1' }, 100);
			}, 1300);
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		};

		this.markUp = function(scope) {
			var content = scope.Preview;
			if (!content) "";

			var categories = scope.categories;
			if (!categories) {
				return content;
			}

			for (var c=0; c < categories.length; c++) {
				var category = categories[c];
				for (var w=0; w < category.categoryWords.length; w++) {
					var cword = category.categoryWords[w];
					cword.guid = this.guid();
					cword.text = (cword.translation || cword.word) + " " +  (cword.explanation || "");   // + (cword.explanation ? "  -  " : "")
					cword.text1 = (cword.translation || cword.word) + " ";  /// + (cword.explanation ? "  -  " : "");
					cword.text2 = (cword.explanation || "");
				}
			}

			scope.guid = this.guid();
			this.currentGuid = scope.guid;
			
			
			var categories = scope.categories;
			var marks = [];
			
			for (var c=0; c < categories.length; c++) {
				var category = categories[c];
				var categoryName = category.categoryName;
				for (var w=0; w< category.categoryWords.length; w++) {
					var cword = category.categoryWords[w];
					cword.category = categoryName;
					cword.length = cword.word.length;
					marks.push(cword);
				}
			}
			
			var idxMarks = [];
			
			for (var m=0; m < marks.length; m++) {
				var mark = marks[m];
				for (var ind=0; ind < (mark.startIndex || mark.startIndexes).length; ind++) {
					idxMarks.push( { index : (mark.startIndex || mark.startIndexes)[ind], mark : mark} );
				}
			}

			idxMarks = idxMarks.sort( function(a, b) {return a.index - b.index } );

			var guid = this.guid();
			if (!this.marksMap[scope.guid]) this.marksMap[scope.guid] = {};
			
			
			var textChanks = [];
			var text = scope.Preview;
			var shift = 0;
			for (var im=0; im < idxMarks.length; im++) {
				var mrk = idxMarks[im];
				if (shift && mrk.index <= shift) continue;
				this.marksMap[scope.guid][mrk.mark.guid] = mrk.mark;
				this.marksMap[scope.guid][mrk.mark.guid].style = function () {
					return {
						'background-color': this.highlighted ? (this.score ? ( this.score < 5 ? 'yellow' : (this.score < 8 ? "orange" : "#FFA6A1")) : "#aaa") : '#CAE6F4'
					};
				};
				this.marksMap[scope.guid][mrk.mark.guid].expStyle = function () {
					return {
						'background-color': this.highlighted ? (this.score ? ( this.score < 5 ? 'yellow' : (this.score < 8 ? "orange" : "#FFA6A1")) : "#aaa") : 'transparent'
					};
				};
				
				var prev = text.substring(0, mrk.index - shift);
				if (prev) {
					textChanks.push( {text : prev, mark: null} );
				}
				var markWord = text.substr(mrk.index - shift, mrk.mark.length);
				textChanks.push( {text : markWord, mark: this.marksMap[scope.guid][mrk.mark.guid]} );
				text = text.substring(mrk.index - shift + mrk.mark.length);
				shift = mrk.index + mrk.mark.length;
			}
			if (text) {
				textChanks.push( {text : text, mark: null} );
			}

			var markedContent = "";
			for (var ch=0; ch < textChanks.length; ch++) {
				var chank = textChanks[ch];
				
				if (chank.mark) {
					var html = "<span ng-style='$ctrl.marksMap[\"" + scope.guid + "\"][\"" + chank.mark.guid + "\"].style()' data-category=\"" + chank.mark.category + "\" data-word=\"" + chank.mark.word + "\" style=' cursor:pointer'  ng-click='$ctrl.markClick(\"" + scope.guid + "\", \""+ chank.mark.guid +"\")'>" + chank.text + "</span>";
					markedContent = markedContent + html;
				}
				else {
					markedContent = markedContent + chank.text;
				}
			}
			return markedContent;
		};

		this.init = function() {
			if (!self.results || !self.results.Values || self.results.Values.length == 0) {
				return;
			}
			
			var markedText = this.markUp(self.results.Values[0]);
			var previewSpan = $("#previewSpan");
			 previewSpan.html(markedText);
			 
			 try{
	             $compile(previewSpan.contents())($scope);
			 }
			 catch(e){
					 //Mattan: nothing to do...
			 }

			this.markClick();
		};
		
		var groupBy = function(xs, key) {
		  return xs.reduce(function(rv, x) {
			(rv[x[key]] = rv[x[key]] || []).push(x);
			return rv;
		  }, {});
		};

		this.countOpenedGroups = function() {
			var currentExplanations = this.currentExplanations;
			var currentExplanationsArr = Object.keys(currentExplanations).map(function (key) {return currentExplanations[key]});
			var n = currentExplanationsArr.filter(function(g) { return g.open}).length;
			var percent = !n ? "30px" : ((100 / n) + "%");
			return percent;
		};
		
		this.markClick = function(contentGuid, guid) {
			var store = this.marksMap;
			if (contentGuid) {
				if (this.lastMarked && this.lastMarked != store[contentGuid][guid]) this.lastMarked.highlighted = false;
				store[contentGuid][guid].highlighted = !store[contentGuid][guid].highlighted;
				this.lastMarked = store[contentGuid][guid].highlighted ? store[contentGuid][guid] : null;
			}
			else {
				contentGuid = this.currentGuid;
			}

			var marks = store[contentGuid];
			var categoriesArr = Object.keys(marks).map(function (key) {return marks[key]});
			var categories = groupBy(categoriesArr, 'category');
			var categoriesUI = {}
			for (var cat in categories) {
				var category = categories[cat];
				categoriesUI[cat] = {items : category };
				var isActiveGroup = true;
				categoriesUI[cat].open = isActiveGroup;
			}
			this.currentExplanations = categoriesUI;
		};

		this.explanationClick = function(obj) {
			if (!obj.highlighted) {
				if (this.lastMarked && this.lastMarked != obj) this.lastMarked.highlighted = false;
				obj.highlighted = !obj.highlighted;
				this.lastMarked = obj;
				this.next(obj);
			}
			else {
				this.next(obj);
			}
		};
	}
	
	function ciferModel() {
		  this.data = {};
	}
	
})();