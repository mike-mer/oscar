(function () {
    'use strict';

    function createGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    angular.module("oscar").component('profile', {
        bindings: {

        },
        templateUrl: 'components/profile/profile.html',
        controller: profileController
    }).service('profileModel', profileModel);

    profileController.$inject = ['profileModel', 'api', '$q', 'appConfig', '$http', '$sce', '$timeout', '$state'];
    function profileController(profileModel, apiService, $q, appConfig, $http, $sce, $timeout, $state) {
        var self = this;
        self.CheckboxPalette = ['secondary', 'purple', 'blue', 'orange', 'red'];
        self.xenonPalette = ['#68b828', '#7c38bc', '#0e62c7', '#fcd036', '#c0392b', '#00b19d', '#ff6264', '#f7aa47'];
        self.baseImageUrl = location.origin;

		function getSourceStyleByName(name) {
			switch (name) {
				case "facebook" :
					return { icon: "socicon-facebook", background : "#3e5b98", url : "https://www.facebook.com/" };
				case "twitter" :
					return { icon: "socicon-twitter", background : "#4da7de", url : "https://twitter.com/" };
				case "googleplus" :
					return { icon: "socicon-googleplus", background : "#dd4b39" };
				case "linkedin" :
					return { icon: "socicon-linkedin", background : "#3371b7" };
				case "windows" :
					return { icon: "fa-windows", background : "#dd4b39" };
				case "bing" :
					return { icon: "custom-bing", background : "transparent" };
				case "google" :
					return { icon: "custom-google", background : "transparent" };
				case "sixGil" :
					return { icon: "socicon-envato", background : "#800080" };
				default :
					return { icon: "", background : "#3371b7" };
			}
		}

		self.clickFriend = function(friend) {
			var id = self.friendId;
			$state.go("es_gallery");
			return;

			var categoriesPrm = {};
			self.categories.forEach(function(d) {
				if (!categoriesPrm[d.dictionaryName]) {
					categoriesPrm[d.dictionaryName] = [d.categoryName]
				}
				else {
					categoriesPrm[d.dictionaryName].push(d.categoryName);
				}
			});
			
			
			if (friend && friend.users && friend.users.length && friend.users[0].userId && friend.users[0].networkName) {
				var request = { "users":[{"userId":friend.users[0].userId,"networkName":friend.users[0].networkName}],"dictionaries":categoriesPrm};
				var profileApiData = { module: "profile", method: "getEntitySummaryIdForce", data: { data: request } };
				apiService.execute(profileApiData).then(function (response) {
					$state.go("es_gallery");
				});	
			}
		};

        self.sourceId = $state.params.id;// 136932
        var profileApiData = { module: "profile", method: "getEntitySummary", data: { id: self.sourceId } };
        apiService.execute(profileApiData).then(function (response) {
            self.profile = response.data;
			
			if (self.profile && self.profile.informationCounters ) {
				self.profile.informationCounters.forEach(function(p) {
					p.counterName = p.counterName[0].toUpperCase() + p.counterName.substring(1);
				})
			}
			
			if (self.profile && self.profile.users) {
				self.profile.users.forEach(function(p) {
					p.style = getSourceStyleByName(p.networkName);
				})
			}
			
            response.data.checkins = response.data.checkins || [];
            response.data.friends = response.data.friends || [];
            response.data.dominantFamilies = response.data.dominantFamilies || [];

            self.graphData = { nodes: [{ id: self.sourceId, extra: { name: response.data.fullName || "source", image:  (response.data.mainProfilePicture ? self.baseImageUrl + "/EntitiesDiscovery/images/" + response.data.mainProfilePicture.imageId : "") }, }] };

            self.graphData.nodes = self.graphData.nodes.concat(response.data.friends.map(function (f) {
                return { id: f.userId.toString(), extra: { networkName: f.networkName, name: f.fullName, image: self.baseImageUrl + "/EntitiesDiscovery/images/" + f.image.imageId }, };
            }));

            self.graphData.orgNodes = self.graphData.nodes;

            var counter = 0;
            self.graphData.links = response.data.friends.map(function (l) {
                return { id: counter++, from: self.sourceId.toString(), to: l.userId.toString(), extra: { type: l.networkName }, };
            });
            setGraphData();

            var wordsDataSource = response.data.dominantFamilies.map(function (df) { return { text: df.familyName, weight: df.familySize }; });

			$('#TagCloud').jQCloud(wordsDataSource, { delay: 50 });

			setTimeout(function () {
                var geocoder = new google.maps.Geocoder();
                var mapOptions = {
					  zoom: 4,
					  center: {lat: 31, lng: 35},
					  mapTypeControl: true
                };
				if (response.data.checkins && response.data.checkins.length) {
					mapOptions.center = new google.maps.LatLng(response.data.checkins[0].latitude, response.data.checkins[0].longtitude)	
				}
				
                function centerMarkers(markers, map) {
					var bounds = new google.maps.LatLngBounds();
					for(i=0; i<markers.length; i++) {
					   bounds.extend(markers[i].getPosition());
					}

					//center the map to the geometric center of all markers
					map.setCenter(bounds.getCenter());

					map.fitBounds(bounds);

					//remove one zoom level to ensure no marker is on the edge.
					map.setZoom(map.getZoom()-1); 

					// set a minimum zoom 
					// if you got only 1 marker or all markers are on the same address map will be zoomed too much.
					if(map.getZoom()> 15){
					  map.setZoom(15);
					}
				}
				
                var mapEl = document.getElementById('map-1');
                mapEl.style.height = '550px';
                var map = new google.maps.Map(mapEl, mapOptions);

				var markers = [];
				var info_window = new google.maps.InfoWindow({content: ''});
				
                for (var i in response.data.checkins) {
                    var checkin = response.data.checkins[i];
                    var marker = new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng(checkin.latitude, checkin.longtitude),
						title: results[0].formatted_address,
						icon: "styles/images/checkin.png"
                    });
						marker.note = results[0].formatted_address;
						google.maps.event.addListener(marker, 'click', function() {
							info_window.content = this.note;
							info_window.open(map, this);
						});
					markers.push(marker);
                }
				centerMarkers(markers, map);

				for (var i in response.data.citiesVisited) {
                    var checkin = response.data.citiesVisited[i];
					if (checkin.placeName) {
						geocoder.geocode( { 'address': checkin.placeName}, function(results, status) {
							var marker = new google.maps.Marker({
								map: map,
								position: results[0].geometry.location,
								title: results[0].formatted_address,
								icon: "styles/images/city.png"
							});
								marker.note = results[0].formatted_address;
								google.maps.event.addListener(marker, 'click', function() {
									info_window.content = this.note;
									info_window.open(map, this);
								});
							markers.push(marker);
							centerMarkers(markers, map)
						});
					}
				}
				
				
				for (var i in response.data.placesLived) {
                    var checkin = response.data.placesLived[i];
					if (checkin.placeName) {
						geocoder.geocode( { 'address': checkin.placeName}, function(results, status) {
							var marker = new google.maps.Marker({
								map: map,
								position: results[0].geometry.location,
								title: results[0].formatted_address,
								icon: "styles/images/place.png"
							});
								marker.note = results[0].formatted_address;
								google.maps.event.addListener(marker, 'click', function() {
									info_window.setContent("<span style='font-size:20px; display:inline-block'>" + this.note + "</span>");
									info_window.open(map, this);
								});
							markers.push(marker);
							centerMarkers(markers, map);
						});
					}
				}
				initSlider();
            }, 200);
        });

        function nodeClickFn(event, args) {
            var selectedNode = args.selection[0].data;
            console.log(selectedNode);
            
            var categoriesPrm = {};
			self.categories.forEach(function(d) {
				if (!categoriesPrm[d.dictionaryName]) {
					categoriesPrm[d.dictionaryName] = [d.categoryName]
				}
				else {
					categoriesPrm[d.dictionaryName].push(d.categoryName);
				}
			});
            
            var profileApiData = { module: "profile", method: "getEntitySummaryId", data: { data: { users: [{ "userId": selectedNode.id, "networkName": selectedNode.extra.networkName }],"dictionaries":categoriesPrm } } };
            apiService.execute(profileApiData).then(function (response) {

                self.friendId = response.data.searchKey;
                var profileApiData = { module: "profile", method: "getEntitySummary", data: { id: self.friendId } };
                apiService.execute(profileApiData).then(function (response) {
                    self.selectedFriendProfile = response.data;
                });

                apiService.execute({ module: "profile", method: "getEntityStatisticsResults", data: { id: self.friendId } }).then(function (response) {
                    console.log(response.data);
                    self.friendCategories = response.data;
                    for (var i in self.friendCategories) {
                        self.friendCategories[i].isSelected = true;
                    }
                    self.dictFriendFilterChanged();
                });
                console.log(response);
                return response.data;
            });
        }

        function setGraphData() {
            self.graphControl = SocialSetGraph(self.graphData, self.sourceId, nodeClickFn);
        }

        apiService.execute({ module: "profile", method: "getAnalysisPerTimeResults", data: { id: self.sourceId } }).then(function (response) {
            var colors = ['#68b828', '#7c38bc', '#40bbea'];
            var idx = -1;
            var series = [];
            var all_data_sources = [];
            var newPropNameDict = {};
            for (var i in response.data.data) {
                var itm = response.data.data[i];

                for (var propName in itm) {
                    if (propName === 'period' || !itm.period)
                        continue;

                    for (var innerPropName in itm[propName]) {
                        var parseDate = itm.period.split("-");
                        var newItm = { time: new Date(parseDate[0], parseDate[1], 1), };
                        var newPropName = (propName + "_" + innerPropName).toLowerCase().replace(' ', '');
                        newItm[newPropName] = itm[propName][innerPropName];
                        all_data_sources.push(newItm);
                        if (!newPropNameDict[newPropName]) {
                            series.push({ argumentField: "time", valueField: newPropName, color: colors[++idx], opacity: .65 });
                            newPropNameDict[newPropName] = true;
                        }
                    }
                }
            }

            $("#range-chart").dxRangeSelector({
                dataSource: all_data_sources,
                size: {
                    height: 140
                },
                chart: {
                    series: series
                },

                selectedRangeChanged: function (e) {
                    var filter = {};
                    for (var i in all_data_sources[0]) {
                        filter[i] = [];
                    }

                    $.map(all_data_sources, function (arg, i) {
                        if (date("U", e.startValue) <= date("U", arg.time) && date("U", e.endValue) >= date("U", arg.time)) {
                            for (var i in all_data_sources[0]) {
                                filter[all_data_sources[0][i]].push({
                                    time: arg.time,
                                    reqs: arg[all_data_sources[0]]
                                });
                            }
                        }
                    });

                    for (var i in all_data_sources[0]) {
                        $('#' + all_data_sources[0][i]).dxChart('instance').option('dataSource', filter[all_data_sources[0][i]]);
                    }
                }
            });
            $("#range-chart").data("dxRangeSelector").render();
        });
		
		
        apiService.execute({ module: "profile", method: "getEntityStatisticsResults", data: { id: self.sourceId } }).then(function (response) {
            self.categories = response.data;
			self.categoriesAll = [{categoryName: "All", isSelected: true}].concat(angular.copy(self.categories));
			self.categories = self.categories.map(function(c) {
				if (!c.usersCount) c.usersCount = 0;
				return c;
			});
            apiService.execute({ module: "profile", method: "getEntityStatisticsLinks", data: { id: self.sourceId } }).then(function (linksResponse) {
                for (var i in self.categories) {
                    self.categories[i].isSelected = true;

                    var currDict = linksResponse.data.filter(function (dict) { return dict.categoryName === self.categories[i].categoryName; })[0];
                    if (currDict) {
                        self.categories[i].users = currDict.links.map(function (lnk) { return lnk.userId; });
                        self.categories[i].usersCount = self.categories[i].users.length;
                    }
                }

                var paletteIndex = -1;
				
                $("#catChart").dxChart({
                    dataSource: self.categories,
                    barWidth: 0.5,
                    rotated: true,
                    legend: {
                        visible: false
                    },
					argumentAxis: {
                        label: {
                            customizeText: function(e) {
                                return "";
                            }
                        }
					},				
                    series: {
                        argumentField: "categoryName",
                        valueField: "usersCount",
                        type: "bar"
                    }, customizePoint: function () {
                        paletteIndex++;
                        if (self.xenonPalette.length === paletteIndex)
                            paletteIndex = 0;
                        var color = self.xenonPalette[paletteIndex];

                        return {
                            color: color
                        };
                    },
                    tooltip: {
                        enabled: true,
                        location: "edge",
                        customizeTooltip: function (arg) {
                            return {
                                text: arg.argumentText + " - " + arg.valueText
                            };
                        }
                    }
                });
                self.dictFilterChanged();
            });
        });
		
        apiService.execute({ module: "profile", method: "getEntityStatisticsContent", data: { id: self.sourceId } }).then(function (response) {
            response.data.timelineItems = response.data.timelineItems || [];
            self.posts = response.data.timelineItems;
			
			self.posts.forEach(function(p) {
				p.style = getSourceStyleByName(p.networkName);
			});
			
            var expl = {};
            for (var i in self.posts) {
                var tl = self.posts[i];

                var postMarks = [];
                if (!tl.categories)
                    continue;

                for (var catIdx in tl.categories) {
                    var cat = tl.categories[catIdx];

                    if (!expl[cat.categoryName]) {
                        expl[cat.categoryName] = { count: 0, words: {} };
                    }
                    for (var catWordIdx in cat.categoryWords) {
                        var catWord = cat.categoryWords[catWordIdx];

                        if (!expl[cat.categoryName].words[catWord.word]) {
                            expl[cat.categoryName].count++;
                            expl[cat.categoryName].words[catWord.word] = {categoryScore : cat.categoryScore, score: catWord.score, translation: catWord.translation || catWord.word, explanation : catWord.explanation, count: 0, instances: [] };
                        }

                        for (var wordStartIdx in catWord.startIndexes) {
                            var postMark = angular.copy(catWord);
                            postMark.startIndexes = null;
                            postMark.startIdx = catWord.startIndexes[wordStartIdx];
                            postMark.id = createGuid();
							postMark.categoryName = cat.categoryName;
							
							expl[cat.categoryName].words[catWord.word].style = function () {
																	//debugger;
								return {
									'background-color': this.highlighted ? (this.score ? ( this.score < 5 ? 'yellow' : (this.score < 8 ? "orange" : "#FFA6A1")) : "#aaa") : '#CAE6F4'
								};
							};
							expl[cat.categoryName].words[catWord.word].expStyle = function () {
								return {
									'background-color': this.highlighted ? (this.score ? ( this.score < 5 ? 'yellow' : (this.score < 8 ? "orange" : "#FFA6A1")) : "#aaa") : 'transparent'
								};
							};

                            expl[cat.categoryName].words[catWord.word].instances.push(postMark.id);
                            postMarks.push(postMark);
                        }
                        expl[cat.categoryName].words[catWord.word].count += catWord.startIndexes.length;
                    }
                }
				self.postMarks = postMarks;
				
                var postParts = [];
                var lastIdx = 0;
                for (var markIdx in postMarks) {
                    if (lastIdx < postMarks[markIdx].startIdx)
                        postParts.push(tl.content.substr(lastIdx, postMarks[markIdx].startIdx - 1));

					var color = (postMarks[markIdx].score <= 5 ? 'yellow' : (postMarks[markIdx].score <= 8 ? 'orange' : 'red'));
                    postParts.push("<span style='cursor:pointer' ng-click='$ctrl.wordClick(\"" + postMarks[markIdx].categoryName + "\", \"" + postMarks[markIdx].word + "\")' mark-id='" + postMarks[markIdx].id + "' ng-style='$ctrl.currentExplanations[\"" + postMarks[markIdx].categoryName + "\"].words[\"" + postMarks[markIdx].word + "\"].style()'>" + tl.content.substr(postMarks[markIdx].startIdx, postMarks[markIdx].word.length) + "</span>");
                    lastIdx += postMarks[markIdx].startIdx + postMarks[markIdx].word.length + 1;
                }
                postParts.push(tl.content.substr(lastIdx));

				tl.content = postParts.join();
            }
            self.currentExplanations = expl;
        });
		
        self.toDate = function (num) {
            return new Date(num);
        };

		self.dictFilterPostsChanged = function(category) { 
		
			self.categoriesAll.forEach(function(c) {
				c.isSelected = (c.categoryName == category.categoryName ? true : false);
			});
			
            for (var i in self.posts) {
				var toShow = (category.categoryName == "All") || (self.posts[i].categories && ( self.posts[i].categories && self.posts[i].categories.some(function (c) { return category.categoryName == c.categoryName }) ) )
                self.posts[i].hidden = !toShow;
            }
		};

		self.dictFilterChanged = function (category) {
            var selectedCats = self.categories.filter(function (c) { return !!c.isSelected });
            if (!self.graphData)
                return;

            var allUserIds = selectedCats.reduce(function (arr, c) { return arr.concat(c.users) }, []);

            for (var i in self.graphData.nodes) {
                var currId = self.graphData.nodes[i].id;
                if (allUserIds.indexOf(self.graphData.nodes[i].id) === -1 && self.categories.length != selectedCats.length)
                    self.graphControl.hideNode(currId);
                else
                    self.graphControl.showNode(currId);
            }
            var catNames = selectedCats.map(function (c) { return c.categoryName; });

            for (i in self.posts) {
                self.posts[i].hidden = self.categories.length != selectedCats.length && (!self.posts[i].categories || !self.posts[i].categories.some(function (c) { return catNames.indexOf(c.categoryName) !== -1; }));
            }
        };

        self.dictFriendFilterChanged = function () {
            friendGauge.values(self.friendCategories.map(function (c) { return c.categoryPercentage }));
        };
        self.clearCatFilter = function () {
            for (i in self.categories) {
                self.categories[i].isSelected = true;
            }
        };
        self.$onChanges = function (changesObj) {
            console.log(5675, changesObj);
        };
        var groupBy = function (xs, key) {
            return xs.reduce(function (rv, x) {
                (rv[x[key]] = rv[x[key]] || []).push(x);
                return rv;
            }, {});
        };

        self.guid = function () {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        };

        self.marksStore = {};
        self.marksMap = {};
        self.wordsMap = {};

        self.countOpenedGroups = function () {
            var currentExplanations = self.currentExplanations;
            var currentExplanationsArr = Object.keys(currentExplanations).map(function (key) { return currentExplanations[key] });
            var n = currentExplanationsArr.filter(function (g) { return g.open }).length;
            var percent = !n ? "30px" : ((100 / n) + "%");
            return percent;
        };

        self.markClick = function (contentGuid, guid) {
            var store = self.marksMap;
            self.fullMode = true;

            if (contentGuid) {
                self.fullMode = false;
                if (!self.fullMode) {
                    if (self.lastMarked && self.lastMarked != store[contentGuid][guid]) {
                        self.lastMarked.highlighted = false;
                    }
                    if (self.lastMarked) {
                        self.wordsMap[self.lastMarked.category][self.lastMarked.word].highlighted = false;
                    }
                    store[contentGuid][guid].highlighted = !store[contentGuid][guid].highlighted;
                    self.lastMarked = store[contentGuid][guid].highlighted ? store[contentGuid][guid] : null;
                }
                else {
                    if (self.lastMarked && self.lastMarked.category != store[contentGuid][guid].category && self.lastMarked.word != store[contentGuid][guid].word) {
                        self.wordsMap[self.lastMarked.category][self.lastMarked.word].highlighted = false;
                    }
                    self.wordsMap[store[contentGuid][guid].category][store[contentGuid][guid].word].highlighted = !self.wordsMap[store[contentGuid][guid].category][store[contentGuid][guid].word].highlighted;
                    self.lastMarked = self.wordsMap[store[contentGuid][guid].category][store[contentGuid][guid].word].highlighted ? store[contentGuid][guid] : null;
                }

            } else {
                if (self.lastMarked) {
                    self.lastMarked.highlighted = false;
                    self.lastMarked = null;
                }
            }

            if (self.lastMarked == null && contentGuid) {
                contentGuid = null;
                self.fullMode = true;
            }

            var marks, categoriesArr;

            if (contentGuid) {
                marks = store[contentGuid];
                categoriesArr = Object.keys(marks).map(function (key) { return marks[key] });
            } else {
                var cotegoriesArrJoin = [];
                for (var catN in store) {
                    var ctgr = store[catN];
                    var ctgrArr = Object.keys(ctgr).map(function (key) { return ctgr[key] });
                    ctgrArr = ctgrArr.filter(function (item) { return cotegoriesArrJoin.filter(function (i) { return i.category == item.category && i.word == item.word }).length == 0 });
                    cotegoriesArrJoin = cotegoriesArrJoin.concat(ctgrArr)
                }
                categoriesArr = cotegoriesArrJoin;
            }

            var categories = groupBy(categoriesArr, 'category');
            var categoriesUI = {};
            for (var cat in categories) {
                var category = categories[cat];
                categoriesUI[cat] = { items: category };
                var isActiveGroup = category.filter(function (c) { return c.highlighted || c.category == self.dictionary }).length > 0;
                categoriesUI[cat].open = isActiveGroup;
            }
            self.currentExplanations = categoriesUI;
        };

		function openGategory(c) {
			var expls = self.currentExplanations;
			for (var e in expls) {
				var exp = expls[e];
				exp.open = (c == e);
			}
		}
		
		this.lastMark = null;
        self.nextInstance = function (word, w, g, k) {
			
			if (!self.lastMark) {
				self.lastMark = self.currentExplanations[k].words[w];
				self.lastMark.highlighted = true;
			}
			else {
				if (self.lastMark == self.currentExplanations[k].words[w]) {
					
				}
				else {
					self.lastMark.highlighted = false;
					self.lastMark = self.currentExplanations[k].words[w];
					self.lastMark.highlighted = true;
				}				
			}
			
            if (!word.currPointer && word.currPointer !== 0)
                word.currPointer = -1;

            word.currPointer++;

            if (word.currPointer == word.count)
                word.currPointer = 0;


            var area = $(".cbp_tmtimeline");
            var nextMarkId = word.instances[word.currPointer];
            var match = $("[mark-id='" + nextMarkId + "']");
            var matchTop = match.offset().top;

            console.log(matchTop, area[0].scrollTop, area.offset().top, match.scrollTop(), match.position().top, matchTop + area[0].scrollTop, matchTop - area[0].scrollTop);
            area.animate({ 'scrollTop': (matchTop + area[0].scrollTop - area.offset().top - 30) + 'px' }, '500');

            var animationInterval = setInterval(function () {
                if (match.css('opacity') == 1) {
                    match.animate({ 'opacity': '0' }, 220);
                } else {
                    match.animate({ 'opacity': '1' }, 220);
                }
            }, 230);

            setTimeout(function () {
                clearInterval(animationInterval);
                match.animate({ 'opacity': '1' }, 100);
            }, 1300);
        };

        self.runFullLA = function () {
            var profileApiData = { module: "profile", method: "runFullLA", data: { id: self.sourceId } };
            apiService.execute(profileApiData).then(function (response) {
            });
        }

    }

    function profileModel() {
        this.data = {};
    }
})();
