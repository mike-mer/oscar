(function () {
	'use strict';

	angular.module("oscar").component('esGallery', {
		bindings: {},
		templateUrl: 'components/es/es-gallery.html',
		controller: edGalleryController
	}).service('esGalleryModel', esGalleryModel);

	edGalleryController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'esGalleryModel', 'api', '$state', '$ocLazyLoad', '$timeout'];
	function edGalleryController(generalService, api, popups, appConfig, esGalleryModel, apiService, $state, $ocLazyLoad, $timeout) {
		var self = this;

        function ensurePreloadModules(array, callback) {
            $(".page-loading-overlay").removeClass("loaded");

            return $ocLazyLoad.load(array)
                .then(function() {
                    $(".page-loading-overlay").addClass("loaded");
                    callback();
                });
        }

        this.info = function(task) {
            ensurePreloadModules(['components/ed/ed-results.js'], function() {
                var url = task.collectionRequest.requestValuesList[2].value;
                var urlSplit = url.split("/");
                var id = urlSplit.pop();

                popups.showPopup( {
                  title : "NSD Info",
                  url : "<ed-results ed-id='" + id + "'></ed-results>",
                  width : '90%',
                  height : '90%',
                  type : 'none',
                  buttons : []
                } );
            });
        };

        this.getReference = function(data, index, idOnly) {
            var url = data.collectionRequest.requestValuesList[index].value;
            if (!idOnly) return url;
            var urlSplit = url.split("/");
            var id = urlSplit.pop();
            return id;

        };

        this.nextPage = function(deferredObj) {
            self.getGextPage(false, function() {
                return deferredObj.resolve();
            });
        };

        function getSourceStyleByName(name) {
            switch (name) {
                case "facebook" :
                    return { icon: "socicon-facebook", background : "#3e5b98", url : "https://www.facebook.com/" };
                case "twitter" :
                    return { icon: "socicon-twitter", background : "#4da7de", url : "https://twitter.com/" };
                case "googleplus" :
                    return { icon: "socicon-googleplus", background : "#dd4b39" };
                case "linkedin" :
                    return { icon: "socicon-linkedin", background : "#3371b7" };
                case "windows" :
                    return { icon: "fa-windows", background : "#dd4b39" };
                case "bing" :
                    return { icon: "custom-bing", background : "transparent" };
                case "google" :
                    return { icon: "custom-google", background : "transparent" };
                case "sixGil" :
                    return { icon: "socicon-envato", background : "#800080" };
                default :
                    return { icon: "", background : "#3371b7" };
            }
        }

        this.tasks = [];
        this.startPage = 0;
        this.pageSize = 30;
        this.finished = false;
        this.getGextPage = function(reset, callback) {
            if (self.finished) {
                return;
            }
            if (reset) {
                self.startPage = 0;
                self.tasks = [];
            }

            var apiData = { module: "taskManager", method: "getTasks", data: {type: "EntitySummary", size: self.pageSize, page: self.startPage} };
            apiService.execute(apiData).then(function(response) {
                if (!response || response.length == 0) {
                    self.finished = true;
                }

                if (response && response.length) {
                    response = response.map(function(data) {
                        data.collectionRequest.name = decodeURIComponent(data.collectionRequest.name);
                        var json = angular.fromJson(data.collectionRequest.name);
                        var network = json[0].networkName;
                        var user = json[0].userId;
                        var style = getSourceStyleByName(network);
                        style.user = user;
                        if (json[0].fullName) style.fullName = json[0].fullName;
                        if (json[0].image && json[0].image.imageId) style.image = "/EntitiesDiscovery/images/" + json[0].image.imageId;
                        data.collectionRequest.style = style;
                        return data;
                    });
                }

                self.tasks = self.tasks.concat(response);
                self.startPage += self.pageSize;
                if (callback) {
                    callback();
                }
            });
        };

        this.getGextPage();
        this.expandRow = function(row, element) {
            $(element).closest("tr").toggleClass("expanded");
            row.expanded = !row.expanded;

            if (!$(element).closest("tr").hasClass("expanded")) {
                $(element).closest("tr").next().remove();
            }
            else {
                $("<tr class='detailsRow' style='height:50px; background:#6B7884'><td colSpan='7'><span style='float:right'><button style='background: #2eb4e8; border: none; color: white; padding: 5px; font-size: 14px; padding-left: 10px; padding-right: 10px; margin-right: 60px;'>INFO</button></span></td></tr>").insertAfter(element.closest("tr"));
            }
        }
    }

	function esGalleryModel() {
		  this.data = {};
	}
})();
