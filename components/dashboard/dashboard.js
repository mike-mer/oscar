(function () {
  'use strict';

	angular.module("oscar").component('dashboard', {
		bindings: {},
		templateUrl: 'components/dashboard/dashboard.html',
		controller: dashboardController
	}).service('dashboardModel', dashboardModel);

	dashboardController.$inject = ['dashboardModel', 'api', '$compile', '$scope', '$state', '$timeout'];
	function dashboardController(dashboardModel, apiService, $compile, $scope, $state, $timeout) {
		var self = this;
		this.xenonPalette = ['#68b828', '#7c38bc', '#0e62c7', '#fcd036', '#c0392b', '#00b19d', '#ff6264', '#f7aa47'];
		this.graphData = {};
		this.setGraphData = function(data, control) {
			self.graphData = data;
			self.graphControl = control;
		};


		this.selectedCategoriesNotificator = null;
		this.setSelectedCategoriesNotificator = function(subscriber) {
			self.selectedCategoriesNotificator = subscriber;
		};

		this.keepExplorirng = function(selectedNode) {
			var categoriesPrm = {};
			self.categories.forEach(function(d) {
				if (!categoriesPrm[d.dictionaryName]) {
					categoriesPrm[d.dictionaryName] = [d.categoryName]
				}
				else {
					categoriesPrm[d.dictionaryName].push(d.categoryName);
				}
			});

            var profileApiData = { module: "profile", method: "getEntitySummaryId", data: { data: { users: [{ "userId": selectedNode.id, "networkName": selectedNode.extra.networkName }],"dictionaries":categoriesPrm } } };
            apiService.execute(profileApiData).then(function (response) {
                self.friendId = response.data.searchKey;
				$state.go("es_gallery");
            });
		};

		this.updateCategories = function(categories) {
			var selectedCats = categories.filter(function (c) {
				return !!c.isSelected
			});
            var allUserIds = selectedCats.reduce(function (arr, c) {
                return arr.concat(c.users)
            }, []);
            for (var i in self.graphData.nodes) {
                var currId = self.graphData.nodes[i].id;
                if (allUserIds.indexOf(self.graphData.nodes[i].id) === -1 && categories.length != selectedCats.length)
                    self.graphControl.hideNode(currId);
                else
                    self.graphControl.showNode(currId);
            }
		};

		this.updateSelectedFriend = function(info) {
			$timeout(function() {
                if (!info) {
                    self.profile.selectedFriend = null;
                    var categories = self.categories.map(function(c) { c.isSelected = true; return c;} );
                    self.selectedCategoriesNotificator(categories);
                    return;
                }
 				var newCounters = {};
				for (var c in info.extra.counters) {
					var name = c.replace("numOf","");
					var val = info.extra.counters[c];
					newCounters[name] = val;
				}
				info.extra.counters = newCounters;
				info.extra.categories = info.extra.categories || self.defaultCategories;
				self.profile.selectedFriend = info;
			})
		};

		function getSourceStyleByName(name) {
			switch (name) {
				case "facebook" :
					return { icon: "socicon-facebook", background : "#3e5b98", url : "https://www.facebook.com/" };
				case "twitter" :
					return { icon: "socicon-twitter", background : "#4da7de", url : "https://twitter.com/" };
				case "googleplus" :
					return { icon: "socicon-googleplus", background : "#dd4b39" };
				case "linkedin" :
					return { icon: "socicon-linkedin", background : "#3371b7" };
				case "windows" :
					return { icon: "fa-windows", background : "#dd4b39" };
				case "bing" :
					return { icon: "custom-bing", background : "transparent" };
				case "google" :
					return { icon: "custom-google", background : "transparent" };
				case "sixGil" :
					return { icon: "socicon-envato", background : "#800080" };
				default :
					return { icon: "", background : "#3371b7" };
			}
		}

		self.sourceId = $state.params.id;
		var profileApiData = { module: "profile", method: "getEntitySummary", data: { id: self.sourceId } };
		apiService.execute(profileApiData).then(function (response) {
			self.profile = response.data;
			self.profile.selectedFriend = null;

			if (self.profile && self.profile.informationCounters ) {
				self.profile.informationCounters.forEach(function(p) {
					p.counterName = p.counterName[0].toUpperCase() + p.counterName.substring(1);
				})
			}

			if (self.profile && self.profile.users) {
				self.profile.users.forEach(function(p) {
					p.style = getSourceStyleByName(p.networkName);
				})
			}

			response.data.checkins = response.data.checkins || [];
			response.data.friends = response.data.friends || [];
			response.data.dominantFamilies = response.data.dominantFamilies || [];

			init();
		});

		apiService.execute({ module: "profile", method: "getEntityStatisticsResults", data: { id: self.sourceId } }).then(function (response) {
			var categories = response.data;

			var categoriesAll = [{categoryName: "All", isSelected: true}].concat(angular.copy(categories));
			categoriesAll.forEach(function(item) {
				item.name = item.categoryName;
				item.count = item.categoryPercentage;
			});
			self.categoriesAll = categoriesAll;

			categories = categories.map(function(c) {
				if (!c.usersCount) c.usersCount = 0;
				return c;
			});

			apiService.execute({ module: "profile", method: "getEntityStatisticsLinks", data: { id: self.sourceId } }).then(function (linksResponse) {
				for (var i in categories) {
					categories[i].isSelected = true;

					var currDict = linksResponse.data.filter(function (dict) { return dict.categoryName === categories[i].categoryName; })[0];
					if (currDict) {
						categories[i].users = currDict.links.map(function (lnk) { return lnk.userId; });
						categories[i].usersCount = categories[i].users.length;
					}
				}

				categories.forEach(function(item) {
					item.name = item.categoryName;
					item.count = item.categoryPercentage;
				});

				self.defaultCategories = angular.copy(categories).map(function(c) { c.categoryPercentage = c.usersCount = c.count = 0; return c; } )
				self.categories = categories;
			});
		});
	}

	function dashboardModel() {
        this.data = {};
	}
})();
