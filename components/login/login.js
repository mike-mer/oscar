(function () {
  'use strict';
  
	angular.module("oscar").component('login', {
		bindings: {},
		templateUrl: 'components/login/login.html',
	}).service('loginModel', loginModel);
	
	function loginModel() {
		  this.data = {};
	}
	
})();