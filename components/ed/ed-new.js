(function () {
  'use strict';

    angular.module("oscar").component('edNew', {
        bindings: {
            edId : '@'
        },
        templateUrl: 'components/ed/ed-new.html',
        controller: edNewController
    }).service('edNewModel', edNewModel);



    edNewController.$inject = ['api', '$timeout', '$state'];
    function edNewController(apiService, $timeout, $state) {
        var self = this;

        this.searchParams = {
            usernames : {
                facebook: { text: "", isActive : false},
                twitter: { text: "", isActive : false}
            },
            firstName: "",
            middleName: "",
            lastName: "",
            mail: "",
            phone: "",
            sex: "MALE",
            birthdate: "",
            filePath: "",

            WorkPlace : "",
            WorkPlaceDescription: "",
            Education: "",
            EducationDescription: "",
            PlaceLived: "",
            PlaceLivedDescription: "",
            FamilyMember: "",
            Friend: ""
        };

        this.$onInit = function () {
            self.id = null;
            if (self.edId) {
                self.id = self.edId;
            }
        };


        this.search = function() {
            var requestData = buildRequest();
            var apiData = { module: "ED", method: "search", data: {payload: requestData} };
            apiService.execute(apiData).then(function() {
                $state.go("ed_gallery");
            });
        };

        this.validateData = function() {
            if (self.searchParams.lastName || self.searchParams.mail || self.searchParams.phone || self.searchParams.usernames.facebook.text || self.searchParams.usernames.twitter.text){
                return false;
            } else {
                return true;
            }
        };

        function getBase64(dataURI) {
              var BASE64_MARKER = ';base64,';
              var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
              var base64 = dataURI.substring(base64Index);
              return base64;
        }

        function buildRequest() {
            var request = {};
            self.searchParams.birthdate && (request.birthDate = moment(self.searchParams.birthdate, "DD/MM/YYYY").toDate().getTime());
            self.searchParams.usernames.facebook.isActive && ((request.networkName = "facebook") && (request.userId = self.searchParams.usernames.facebook.text));
            self.searchParams.usernames.twitter.isActive && !self.searchParams.usernames.facebook.isActive && ((request.networkName = "twitter") && (request.userId = self.searchParams.usernames.twitter.text));
            self.searchParams.filePath && (request.profilePicture = getBase64(self.searchParams.filePath));
            self.searchParams.firstName && (request.firstName = self.searchParams.firstName);
            self.searchParams.middleName && (request.middleName = self.searchParams.middleName);
            self.searchParams.lastName && (request.lastName = self.searchParams.lastName);
            self.searchParams.mail && (request.emailAddress = self.searchParams.mail);
            self.searchParams.phone && (request.phoneNumber = self.searchParams.phone);
            self.searchParams.sex && (request.gender = self.searchParams.sex);
            self.searchParams.WorkPlaceDescription && (request.workPlaces = [self.searchParams.WorkPlaceDescription]);
            self.searchParams.EducationDescription && (request.education = [self.searchParams.EducationDescription]);
            self.searchParams.PlaceLivedDescription && (request.placesLived = [self.searchParams.PlaceLivedDescription]);
            self.searchParams.FamilyMember && (request.familyMembers = [self.searchParams.FamilyMember]);
            self.searchParams.Friend && (request.friends = [self.searchParams.Friend]);

            return request;
        }

        this.fileNameChanged = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = e.target.result;
                    $timeout(function() {
                        self.searchParams.filePath = img;
                    });
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    }

	function edNewModel() {
		  this.data = {};
	}
})();
