(function () {
	'use strict';
	angular.module("oscar").component('edResults', {
		bindings: {
			edId : '@'
		},
		templateUrl: 'components/ed/ed-results.html',
		controller: edResultsController
	}).service('edResultsModel', edResultsModel);

	edResultsController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'edResultsModel', 'api', '$compile', '$scope', '$state', '$ocLazyLoad'];
	function edResultsController(generalService, api, popups, appConfig, edResultsModel, apiService, $compile, $scope, $state, $ocLazyLoad) {
		var self = this;

		function getSourceStyleByName(name) {
			switch (name) {
				case "facebook" :
					return { icon: "socicon-facebook", background : "#3e5b98" };
				case "twitter" :
					return { icon: "socicon-twitter", background : "#4da7de" };
				case "googleplus" :
					return { icon: "socicon-googleplus", background : "#dd4b39" };
				case "linkedin" :
					return { icon: "socicon-linkedin", background : "#3371b7" };
				case "windows" :
					return { icon: "fa-windows", background : "#dd4b39" };
				case "bing" :
					return { icon: "custom-bing", background : "transparent" };
				case "google" :
					return { icon: "custom-google", background : "transparent" };
				case "sixGil" :
					return { icon: "socicon-envato", background : "#800080" };
				default :
					return { icon: "", background : "#3371b7" };
			}
		}

		this.$onInit = function () {
        self.id = null;
  			if ($state.params && $state.params.id) {
  				self.id = $state.params.id;
  			}
  			else {
  				if (self.edId) {
  					self.id = self.edId;
  				}
  			}
			getData();
		};

        function createGuid() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        }

        this.lines = [];

        this.fillFilter = function(item, index) {
            fillMeter(".filler" + index, item.score == "VERY_HIGH" ? 100 : (item.score == "HIGHEST") ? 100 : (item.score == "HIGH" ? 75 : (item.score == "MEDIUM" ? 50 : (item.score == "LOW" ? 25 : 0))));
        };

        this.categoriesColumns = [];
        this.dictionaryPicked = function() {
            var dictionary = self.selectedDictionary;
            if (!dictionary) return;

            var apiData = { module: "NSD", method: "getDictionary", data: {name : dictionary} };
            apiService.execute(apiData).then(function(response) {
                var categories = response.DictionaryCategory.map(function(d) { return {name: d.CategoryName} });
                self.categoriesColumns.push( {dictionary: dictionary, categories: categories} );
            });
        };

        this.getOpenDictionaries = function() {
            var openDictionaries = [];
            if (self.dictionaries) {
                self.dictionaries.forEach(function(item) {
                    if ( self.categoriesColumns.filter(function(c) { return c.dictionary == item.name}).length == 0 ) {
                        openDictionaries.push(item);
                    }
                });
            }
            return openDictionaries;
        };

        var apiData = { module: "NSD", method: "getDictionaries" };
        apiService.execute(apiData).then(function(response) {
            self.dictionaries = response.map(function(d) { return {name: d} });
        });

        function getData() {
            var apiData = { module: "ED", method: (self.edId ? "query" : "getList"), data: {id : self.id || 1188} };
            apiService.execute(apiData).then(function(response) {
                if (response && response.length && !response.searchResults) {
                    response = response.map(function(r) { return { entityDetailsDTO : r } });
                    response = { searchResults : response, searchImage : {} };
                }

                if (response && response.searchResults && response.searchResults.length != 0) {

                    if (response.searchImage.imageId) self.originalImagePath = appConfig.host + "/EntitiesDiscovery/images/" + response.searchImage.imageId;

                    var results = response.searchResults;
                    results = results.map(function(r) {
                        if (r.entityDetailsDTO && r.entityDetailsDTO && r.entityDetailsDTO.profilePicture && r.entityDetailsDTO.profilePicture.imageId) {
                            r.entityDetailsDTO.profilePicture.imagePath = appConfig.host + "/EntitiesDiscovery/images/" + r.entityDetailsDTO.profilePicture.imageId;
                        }

                        r.style= getSourceStyleByName(r.entityDetailsDTO.networkName);

                        if (r.entityDetailsDTO.placesLived) {
                            r.entityDetailsDTO.placesLivedStr = r.entityDetailsDTO.placesLived.map(function(p) { return p.placeName }).join(" | ");
                        }
                        else {
                            r.entityDetailsDTO.placesLivedStr = "";
                        }

                        if (r.entityDetailsDTO.placesStudied) {
                            r.entityDetailsDTO.placesStudiedStr = r.entityDetailsDTO.placesStudied.map(function(p) { return p.educationPlaceName }).join(" | ");
                        }
                        else {
                            r.entityDetailsDTO.placesStudiedStr = "";
                        }

                        if (r.entityDetailsDTO.workplaces) {
                            r.entityDetailsDTO.workplacesStr = r.entityDetailsDTO.workplaces.map(function(p) { return p.workplaceName }).join(" | ");
                        }
                        else {
                            r.entityDetailsDTO.workplacesStr = "";
                        }

                        r.images = [{},{},{},{},{}];

                        return r;
                    });

                    self.lines = results.map(function(l) { l.guid = createGuid(); return {items : [l]} });

                    $ocLazyLoad.load(['assets/js/xenon-toggles.js']);
                }
            });

        }

        this.draggedItem = null;

        this.dropCallback = function(e, o, obj, root, ind) {
            var people = self.lines;
            var person = obj;
            var index = ind;

            var draggedItem = self.draggedItem;
            var draggedIndex = self.draggedItem.index;

            person.items = person.items.concat(draggedItem.items);
            people.splice(draggedIndex, 1);

            o.draggable.css("left","0");
            o.draggable.css("top","0");
        };

        this.dragCallback = function(e, o, obj, root, ind) {
            obj.index = ind;
            self.draggedItem = obj;
        };

        this.unlink = function(obj, ind, line, pInd) {
            var item = obj.items[ind];
            obj.showDetails = false;
            if (obj.items.length <= 2) {
                obj.showDetailsJoin = false;
            }
            obj.items.splice(ind, 1);
            this.lines.splice(pInd + 1 , 0, {items : [item]} );
        };

        this.execute = function(block) {
            var payload = block.items.map(function(p) { return { "userId": p.entityDetailsDTO.userId, "networkName": p.entityDetailsDTO.networkName } } );

            var dictionaries = {};
            self.categoriesColumns.forEach(function(dic) {
                var selectedCategories = dic.categories.filter(function(c) { return c.isChecked });
                if (selectedCategories.length > 0) {
                    dictionaries[dic.dictionary] = selectedCategories.map(function(c) {return c.name});
                }
            });

            var post = {users : payload, dictionaries : dictionaries};

            var apiData = { module: "profile", method: "getEntitySummaryIdForce", data: { data: post }};
            apiService.execute(apiData).then(function() {
                $state.go("es_gallery");
            });
        };
	}

	function edResultsModel() {
		  this.data = {};
	}
})();
