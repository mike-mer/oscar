(function () {
	'use strict';
	angular.module("oscar").component('edGallery', {
		bindings: {},
		templateUrl: 'components/ed/ed-gallery.html',
		controller: edGalleryController
	}).service('edGalleryModel', edGalleryModel);

	edGalleryController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'edGalleryModel', 'api', '$state', '$ocLazyLoad', '$timeout'];
	function edGalleryController(generalService, api, popups, appConfig, edGalleryModel, apiService, $state, $ocLazyLoad, $timeout) {
		var self = this;

		this.getReference = function(data, index, idOnly) {
			var url = data.collectionRequest.requestValuesList[index].value;
			if (!idOnly) return url;
			var urlSplit = url.split("/");
			var id = urlSplit.pop();
			return id;
		};

		this.nextPage = function(deferredObj) {
			self.getGextPage(false, function() {
				return deferredObj.resolve();
			});
		};

		this.tasks = [];
		this.startPage = 0;
		this.pageSize = 30;
		this.finished = false;
		this.getGextPage = function(reset, callback) {
			if (self.finished) {
				return;
			}
			if (reset) {
				self.startPage = 0;
				self.tasks = [];
			}
			var apiData = { module: "taskManager", method: "getTasks", data: {type: "EntityDiscovery", size: self.pageSize, page: self.startPage} };
			apiService.execute(apiData).then(function(response) {
				if (!response || response.length == 0) {
					self.finished = true;
				}
				response = response.map(function(data) { data.collectionRequest.name = decodeURIComponent(data.collectionRequest.name); return data;});
				self.tasks = self.tasks.concat(response);
				self.startPage += self.pageSize;
				if (callback) {
					callback();
				}
			});
		};

		this.getGextPage();

		this.expandRow = function(row, element) {
			$(element).closest("tr").toggleClass("expanded");
			row.expanded = !row.expanded;

			if (!$(element).closest("tr").hasClass("expanded")) {
				$(element).closest("tr").next().remove();
			}
			else {
				$("<tr class='detailsRow' style='height:50px; background:#6B7884'><td colSpan='7'><span style='float:right'><button style='background: #2eb4e8; border: none; color: white; padding: 5px; font-size: 14px; padding-left: 10px; padding-right: 10px; margin-right: 60px;' onclick='angular.element(this).scope().$ctrl.rowInfo($(this))'>INFO</button></span></td></tr>").insertAfter(element.closest("tr"));
			}
		}
	}

	function edGalleryModel() {
		  this.data = {};
	}

})();
