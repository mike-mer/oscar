(function () {
  	'use strict';
	angular.module("oscar").component('shell', {
		bindings: {},
		templateUrl: 'components/shell/shell.html',
		controller: shellController
	}).service('shellModel', shellModel);

	shellController.$inject = ['shellModel', 'popups', '$rootScope', '$timeout'];
	function shellController(shellModel, popups, $rootScope, $timeout) {
		var self = this;
		this.popups = [];

		self.sideMenuItems = [
			{title: "Dashboard", path: "/oscar2/dashboard.html", isActive: true, icon: "linecons-doc"},
			{title: "NSD", path: "#/nsd/gallery", isActive: true, icon: "linecons-search"},
			{title: "Entity Discovery", path: "#/ed/gallery", isActive: true, icon: "linecons-database"},
			{title: "360 Overview", path: "#/es/gallery", isActive: true, icon: "linecons-graduation-cap"},
			{title: "CIFER", path: "#/cifer", isActive: true, icon: "linecons-note"}
		];
		
		popups.subscribe(function(data) {
			$timeout(function(){
				self.popups = data;
			});
		})
	}

	function shellModel() {
		  this.data = {};
	}

})();



