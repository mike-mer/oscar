(function () {
  'use strict';

	angular.module("oscar").component('nsdGallery', {
		bindings: {},
		templateUrl: 'components/nsd/nsd-gallery.html',
		controller: nsdGalleryController
	}).service('nsdGalleryModel', nsdGalleryModel);

	nsdGalleryController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'nsdGalleryModel', 'api', '$state', '$ocLazyLoad', '$timeout'];
	function nsdGalleryController(generalService, api, popups, appConfig, nsdGalleryModel, apiService, $state, $ocLazyLoad, $timeout) {   
		var self = this;

		this.nextPage = function(deferredObj) {
			self.getGextPage(false, function() {
				return deferredObj.resolve();
			});
		};

		this.tasks = [];
		this.startPage = 0;
		this.pageSize = 30;
		this.finished = false;
		this.getGextPage = function(reset, callback) {
			if (self.finished) {
				return;
			}
			
			if (reset) {
				self.startPage = 0;
				self.tasks = [];
			}
			
			var apiData = { module: "taskManager", method: "getTasks", data: {type: "NSD-Query", size: self.pageSize, page: self.startPage} };
			apiService.execute(apiData).then(function(response) {
				if (!response || response.length == 0) {
					self.finished = true;
				}
				response = response.map(function(data) { data.collectionRequest.name = decodeURIComponent(data.collectionRequest.name); return data;})
				self.tasks = self.tasks.concat(response);
				self.startPage += self.pageSize;
				if (callback) {
					callback();
				}
			});
		};

		this.getGextPage();
		
		function ensurePreloadModules(array, callback) {
			$(".page-loading-overlay").removeClass("loaded");
			
			return $ocLazyLoad.load(array)
				.then(function() { 
						$(".page-loading-overlay").addClass("loaded"); 
						callback();
					});
		}

		this.getReference = function(data, index, idOnly) {
			var url = data.collectionRequest.requestValuesList[index].value;
			if (!idOnly) return url;
			var urlSplit = url.split("/");
			var id = urlSplit.pop();
			return id;
		};

		this.rowInfo = function(row) {
			ensurePreloadModules(['controls/dictionaryTree/dictionaryTree.js', 'components/nsd/nsd-new.js'], function() {
				var url = row.collectionRequest.requestValuesList[2].value;
				var urlSplit = url.split("/");
				var id = urlSplit.pop();
				var path = "nsd/new/" + id;
				
				popups.showPopup( {
					title : "NSD Info",
					url : "<nsd-new nsd-id='" + id + "'></nsd-new>",
					width : '90%',
					height : '90%',
					type : 'none',
					buttons : []
				} );
			});
		};
	}

	function nsdGalleryModel() {
		  this.data = {};
	}
})();
