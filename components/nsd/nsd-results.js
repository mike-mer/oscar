(function () {
  'use strict';


	angular.module("oscar").component('nsdResults', {
		bindings: {},
		templateUrl: 'components/nsd/nsd-results.html',
		controller: nsdResultsController
	}).service('nsdResultsModel', nsdResultsModel);

	nsdResultsController.$inject = ['generalService', 'api', 'popups', 'appConfig', 'nsdResultsModel', 'api', '$compile', '$scope', '$state'];
	function nsdResultsController(generalService, api, popups, appConfig, nsdResultsModel, apiService, $compile, $scope, $state) {
		var self = this;

		this.$onInit = function () {
			var params = $state.params;
			if (params && params.id) {
				self.id = params.id;
			}
			self.getGextPage();
		};

 		this.stopEvent = function(event) {
			event.stopPropagation();
			event.preventDefault();
		};

		this.selectRow = function(row) {
			(self.lastSelected || {}).isSelected = false;
			self.lastSelected = row;
			self.lastSelected.isSelected = true;

			self.init(row);

			if (!this.stuck) {
				self.expandRight();
			}
		};

		this.stuck = true;

		this.expandLeft = function() {
			$(".splitPane").splitPane('lastComponentSize', 0);
		};
		this.expandRight = function() {
			$(".splitPane").splitPane('firstComponentSize', 0);
		};
		this.expandBoth = function() {
			$(".splitPane").splitPane('lastComponentSize', parseInt($(".splitPane").css("width")) / 2);
		};

		this.getIconStyle = function(type) {
			switch (type) {
				case "facebook" :
					return { icon: "socicon-facebook", background : "#3e5b98" };
				case "twitter" :
					return { icon: "socicon-twitter", background : "#4da7de" };
				case "googleplus" :
					return { icon: "socicon-googleplus", background : "#dd4b39" };
				case "linkedin" :
					return { icon: "socicon-linkedin", background : "#3371b7" };
				case "windows" :
					return { icon: "fa-windows", background : "#dd4b39" };
				case "bing" :
					return { icon: "custom-bing", background : "transparent" };
				case "google" :
					return { icon: "custom-google", background : "transparent" };
				case "sixGil" :
					return { icon: "socicon-envato", background : "#800080", title: "Darknet" };
				default :
					return { icon: "", background : "#3371b7" };
			}
		};

		this.nextPage = function(deferredObj) {
			self.getGextPage(false, function() {
				return deferredObj.resolve();
			});
		};

		this.pageIndex = 0;
		this.pageSize = 30;
		this.finished = false;
		this.getGextPage = function(reset, callback) {
			if (self.finished) {
				return;
			}

			if (reset) {
				self.startPage = 0;
				self.results = [];
			}

			var apiData = { module: "NSD", method: "getTableNSD", data: {size: self.pageSize, index: self.pageIndex, id : self.id || "bb1a257da3c148039c4aa33a41ec3e50"} };
			apiService.execute(apiData).then(function(response) {
				if (!response || response.length == 0) {
					self.finished = true;
				}

				var results = response;
				if (!results || !results.Values || results.Values.length == 0) {
					return;
				}

				results.Titles = results.Titles.map(function(t) { (t.name == "totalScore" ? t : {}).caption = "Total"; return t; }).map(function(t) {
					(t.name == "SourceType" ? t : {}).caption = "Source"; (t.name == "SourceType" ? t : {}).name = "name"; return t; }).filter(function(t) { return t.name != "Link" }).sort(function(t) {
						return ["totalScore", "name", "Website"].indexOf(t.name) != -1 ? 0 : 1 }).sort(function(t) { return t.name != 'Website' ? 0 : 1 });

				if (!self.results) {
					self.results = results;
				}
				else {
					self.results.Values = self.results.Values.concat(results.Values);
				}

				self.pageIndex += 1;

				if (callback) {
					callback();
				}
			});
		};

		this.marksMap = {};

		this.next = function(obj, event) {
			if (event) {
				event.stopPropagation();
			}

			if (!obj.highlighted) {
				this.explanationClick(obj);
				return;
			}

			var category = obj.category;
			var word = obj.word;
			var tab = 0;
			if (this.lastFlash && this.lastFlash.category == category && this.lastFlash.word == word) {
				tab = this.lastFlash.tab + 1;
			}

			var matches = $("#previewSpan span[data-category='" + category + "'][data-word='" + word + "']");
			if (tab >= matches.length) {
				tab = 0;
			}

			this.lastFlash = {category : category, word: word, tab : tab};

			var match = matches.eq(tab);
			var top = match.offset().top;

			var area = $("#previewSpanArea");
			var content = $("#previewSpan").eq(0);
			var contentTop = content.offset().top;

			area.animate({ 'scrollTop': (top - contentTop) + 'px' }, '500');


			///////////////////////////////////////////////////////// blink animation ///////////////////////////////////////////////////////////
			var animationInterval = setInterval(function () {
				if (match.css('opacity') == 1) {
					match.animate({ 'opacity': '0' }, 220);
				} else {
					match.animate({ 'opacity': '1' }, 220);
				}
			}, 230);

			setTimeout(function () {
				clearInterval(animationInterval);
				match.animate({ 'opacity': '1' }, 100);
			}, 1300);
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		};

		this.markUp = function(source) {

			function guid() {
				return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
				return v.toString(16);
				});
			}

			var content = source.Preview;
			if (!content) return "";

			var categories = source.categories;
			if (!categories) {
				return content;
			}

			for (var c=0; c < categories.length; c++) {
				var category = categories[c];
				for (var w=0; w < category.categoryWords.length; w++) {
					var cword = category.categoryWords[w];
					cword.guid = guid();
					cword.text = (cword.translation || cword.word) + " " +  (cword.explanation || "");
					cword.text1 = (cword.translation || cword.word) + " ";
					cword.text2 = (cword.explanation || "");
				}
			}

			source.guid = guid();
			this.currentGuid = source.guid;


			var categories = source.categories;
			var marks = [];

			for (var c=0; c < categories.length; c++) {
				var category = categories[c];
				var categoryName = category.categoryName;
				for (var w=0; w< category.categoryWords.length; w++) {
					var cword = category.categoryWords[w];
					cword.category = categoryName;
					cword.length = cword.word.length;
					marks.push(cword);
				}
			}

			var idxMarks = [];

			for (var m=0; m < marks.length; m++) {
				var mark = marks[m];
				for (var ind=0; ind < (mark.startIndex || mark.startIndexes).length; ind++) {
					idxMarks.push( { index : (mark.startIndex || mark.startIndexes)[ind], mark : mark} );
				}
			}
			idxMarks = idxMarks.sort( function(a, b) {return a.index - b.index } );

			var guid = guid();
			if (!this.marksMap[source.guid]) this.marksMap[source.guid] = {};

			var textChanks = [];
			var text = source.Preview;
			var shift = 0;
			for (var im=0; im < idxMarks.length; im++) {
				var mrk = idxMarks[im];
				if (shift && mrk.index <= shift) continue;
				this.marksMap[source.guid][mrk.mark.guid] = mrk.mark;
				this.marksMap[source.guid][mrk.mark.guid].style = function () {
					return {
						'background-color': this.highlighted ? (this.score ? ( this.score < 5 ? 'yellow' : (this.score < 8 ? "orange" : "#FFA6A1")) : "#aaa") : '#CAE6F4'
					};
				};
				this.marksMap[source.guid][mrk.mark.guid].expStyle = function () {
					return {
						'background-color': this.highlighted ? (this.score ? ( this.score < 5 ? 'yellow' : (this.score < 8 ? "orange" : "#FFA6A1")) : "#aaa") : 'transparent'
					};
				};

				var prev = text.substring(0, mrk.index - shift);
				if (prev) {
					textChanks.push( {text : prev, mark: null} );
				}
				var markWord = text.substr(mrk.index - shift, mrk.mark.length);
				textChanks.push( {text : markWord, mark: this.marksMap[source.guid][mrk.mark.guid]} );
				text = text.substring(mrk.index - shift + mrk.mark.length);
				shift = mrk.index + mrk.mark.length;
			}

			if (text) {
				textChanks.push( {text : text, mark: null} );
			}

			var markedContent = "";
			for (var ch=0; ch < textChanks.length; ch++) {
				var chank = textChanks[ch];

				if (chank.mark) {
					var html = "<span ng-style='$ctrl.marksMap[\"" + source.guid + "\"][\"" + chank.mark.guid + "\"].style()' data-category=\"" + chank.mark.category + "\" data-word=\"" + chank.mark.word + "\" style=' cursor:pointer'  ng-click='$ctrl.markClick(\"" + source.guid + "\", \""+ chank.mark.guid +"\")'>" + chank.text + "</span>";
					markedContent = markedContent + html;
				}
				else {
					markedContent = markedContent + chank.text;
				}
			}
			return markedContent;
		};

		this.init = function(source) {
			var markedText = this.markUp(source);
			var previewSpan = $("#previewSpan");
			previewSpan.html(markedText);
            $compile(previewSpan.contents())($scope);
			this.markClick();
		};

		this.markClick = function(contentGuid, guid) {
			var groupBy = function(xs, key) {
			  return xs.reduce(function(rv, x) {
				(rv[x[key]] = rv[x[key]] || []).push(x);
				return rv;
			  }, {});
			};

			var store = this.marksMap;
			if (contentGuid) {
				if (this.lastMarked && this.lastMarked != store[contentGuid][guid]) this.lastMarked.highlighted = false;
				store[contentGuid][guid].highlighted = !store[contentGuid][guid].highlighted;
				this.lastMarked = store[contentGuid][guid].highlighted ? store[contentGuid][guid] : null;
			}
			else {
				contentGuid = this.currentGuid;
			}

			var marks = store[contentGuid];
			var categoriesArr = Object.keys(marks).map(function (key) {return marks[key]});
			var categories = groupBy(categoriesArr, 'category');
			var categoriesUI = {};
			for (var cat in categories) {
				var category = categories[cat];
				categoriesUI[cat] = {items : category };
				var isActiveGroup = true;
				categoriesUI[cat].open = isActiveGroup;
			}
			this.currentExplanations = categoriesUI;
		};

		this.explanationClick = function(obj) {
			if (!obj.highlighted) {
				if (this.lastMarked && this.lastMarked != obj) this.lastMarked.highlighted = false;
				obj.highlighted = !obj.highlighted;
				this.lastMarked = obj;
				this.next(obj);
			}
			else {
				this.next(obj);
			}
		};

	}

	function nsdResultsModel() {
		  this.data = {};
	}

})();