 (function () {

	angular.module("oscar").component('nsdNew', {
		bindings: {
			nsdId : '@'
		},
		templateUrl: 'components/nsd/nsd-new.html',
		controller: nsdNewController
	}).service('nsdNewModel', nsdNewModel);


	nsdNewController.$inject = ['nsdNewModel', 'api', '$q', '$state'];
	function nsdNewController(nsdNewModel, apiService, $q,  $state) {
		var self = this;

		this.$onInit = function () {
			self.id = null;
			if ($state.params && $state.params.id) {
				self.id = $state.params.id;
			}
			else {
				if (self.nsdId) {
					self.id = self.nsdId;
				}
			}
			init();
		};

		function getSourceStyleByName(name) {
			switch (name) {
				case "facebook" :
					return { icon: "socicon-facebook", background : "#3e5b98" };
				case "twitter" :
					return { icon: "socicon-twitter", background : "#4da7de" };
				case "googleplus" :
					return { icon: "socicon-googleplus", background : "#dd4b39" };
				case "linkedin" :
					return { icon: "socicon-linkedin", background : "#3371b7" };
				case "windows" :
					return { icon: "fa-windows", background : "#dd4b39" };
				case "bing" :
					return { icon: "custom-bing", background : "transparent" };
				case "google" :
					return { icon: "custom-google", background : "transparent" };
				case "sixGil" :
					return { icon: "socicon-envato", background : "#800080" };
				default :
					return { icon: "", background : "#3371b7" };
			}
		}

		this.clearDictionary = function() {
			this.selectedDictionary = undefined;
			this.tree.DictionaryCategory = [];
			self.tree = [];
            self.showTree = false;
		};

		this.dateValue = moment().format('DD/MM/YYYY');

		this.repeatChanged = function(range) {
			if (range == "immediate") {
				self.repeatInterval = "repeat interval";
			}
		};

		this.clearData = function() {
			self.id = null;
		};
		
		this.selectAvatar = function(avatar) {
			if (self.id) return;
			
			(avatar != self.lastAvatarSelected ? (self.lastAvatarSelected || {}) : {}).isSelected = false;   
			avatar.isSelected = (avatar == self.lastAvatarSelected ? !avatar.isSelected : true);   
			self.lastAvatarSelected = avatar;
		};

		this.getDictionary = function (name) {
			var apiData = { module: "NSD", method: "getDictionary", data: { name: name }};
			apiService.execute(apiData).then(function(response) {
				self.tree = response;
				self.clearData();
			})
        };

        function buildRequestForSave() {
            var tree_data = self.contract.getTree();
            var avatars = self.avatars.filter(function(a) { return a.isSelected }).map(function(a) {return a.ID});
            var sources = self.sources.filter(function(a) { return a.isSelected }).map(function(a) {return a.type});
            var searces = self.searches.filter(function(a) { return a.isSelected }).map(function(a) {return a.type});
            sources = sources.concat(searces);

            if (tree_data.DictionaryCategory) {
                tree_data.DictionaryCategory = tree_data.DictionaryCategory.filter(function(c) { return c.isPicked });
                tree_data.DictionaryCategory.forEach(function(c) {
                    delete c.isExpanded;
                    delete c.isPicked;
                });
            }

            var request = {
                name : self.queryName,
                query: {
                    Avatar: avatars ? avatars[0] : "",
                    Dictionaries : [tree_data],
                    freeText : self.textSearch,
                    SearchSources : sources
                },
                scheduler : {
                    isPostponed: (self.range == "startat" ? "1" : "0"),
                    postponeDate: (self.range == "startat" ? (3333333) : "0"),
                    priority: "1",
                    recurrent: (self.repeatInterval != "repeat interval" ? "1" : "0"),
                    data: "19"
                }
            };

            if (self.repeatInterval != "repeat interval") {
                request.scheduler.repeatMode = self.repeatInterval;
            }

            return request;
        }

		this.saveDictionary = function() {
  			var data = buildRequestForSave();
			var apiData = { module: "NSD", method: "runSearch", data: { payload: data }};
			apiService.execute(apiData).then(function(response) {
				$state.go("nsd_gallery");
			});
		};


        this.contract = {
            getTree : null
        };

		this.range = "immediate";
		this.currentDate = new Date();
		
		function init() {

            var apiData = { module: "Avatars", method: "getAvatars", data: {}};
			var AvatarsPromise = apiService.execute(apiData);

			apiData = { module: "NSD", method: "getSearchSources", data: {}};
			var msdPromise =  apiService.execute(apiData);

			apiData = { module: "NSD", method: "getDictionaries", data: {}};
			var dictionariesPromise = apiService.execute(apiData);

			apiData = { module: "NSD", method: "getQuery", data: { id : self.id }};
			var queryPromise = self.id ? apiService.execute(apiData) : null;

			$q.all([AvatarsPromise, msdPromise, dictionariesPromise, queryPromise])
			.then(function(response) {
                var avatars = response[0];
				var sources = response[1];
				var dictionaries = response[2];
				var query = response[3];

				self.dictionaries = dictionaries;

				function parseImageStreamToData(imageStream) {
					try {
						var img = "";
						if (imageStream) {
							var u8 = new Uint8Array(imageStream);
							var b64encoded = btoa(String.fromCharCode.apply(null, u8));
							img = "data:image/png;base64," + b64encoded;
						} else {
							img = "assets/images/user-4.png";
						}
					}
					catch(e) {
						img = "assets/images/user-4.png";
					}

					return img;
				}

				avatars = avatars.map(function(a) {
					a.imageData = parseImageStreamToData(a.image);
					a.isSelected = false;
					return a;
				});

				self.avatars = avatars;

				self.sources = [
					{
						type : "facebook",
						isSelected : false,
						style: getSourceStyleByName("facebook"),
						data: "Facebook"
					},
					{
						type : "twitter",
						isSelected : false,
						style: getSourceStyleByName("twitter"),
						data: "Twitter"
					},
					{
						type : "googleplus",
						isSelected : false,
						style: getSourceStyleByName("googleplus"),
						data: "Google+",
						disable: true
					},
					{
						type : "linkedin",
						isSelected : false,
						style: getSourceStyleByName("linkedin"),
						data: "LinkedIn",
						disable: true
					}
				];

				self.searches = [
					{
						type : "bing",
						isSelected : false,
						style: getSourceStyleByName("bing"),
						data: "Bing"
					},
					{
						type : "google",
						isSelected : false,
						style: getSourceStyleByName("google"),
						data: "Google"
					},
					{
						type : "sixGil",
						isSelected : false,
						style: getSourceStyleByName("sixGil"),
						data: "Darknet"
					}
				];

				if (query) {
					self.tree = query.query.Dictionaries.length ? query.query.Dictionaries[0] : query.query.Dictionaries;
					self.queryName = query.name || "no name";
					self.textSearch = query.query.freeText || "no text";
					self.avatars.forEach(function(a) {
						a.isSelected = a.ID == query.query.Avatar;
					});
					self.searches.forEach(function(a) {
						a.isSelected = query.query.SearchSources.indexOf(a.type) != -1;
					});
					self.sources.forEach(function(a) {
						a.isSelected = query.query.SearchSources.indexOf(a.type) != -1;
					});
					
					self.selectedDictionary = query.query.Dictionaries.DictionaryName || (query.query.Dictionaries.length ? query.query.Dictionaries[0].DictionaryName : "");
				}
			});
		}
	}

	function nsdNewModel() {
		  this.data = {};
	}

 })();
