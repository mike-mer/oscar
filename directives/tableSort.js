angular.module('oscar').directive('tableSort', function($window, $q, $parse) {
    return {
  	restrict: 'A',
/*  	scope:{
  		infiniteScroll: '&'
  	}, */
      link: function(scope, element, attrs) {
            var config = $parse(attrs.tableSort)(scope);
            //debugger;
            // /"th > div > div"
            var sortableColumns = element.find(config.columnHeadersSelector);
            sortableColumns.each(function(index, col) {
                $(col).on("click", function() {
                    //debugger;
                    var desc = config.currentSortIndex == index ? !config.columns[index].desc : true;
                    config.columns[index].desc = desc;
                    element.find(".sortedBy").removeClass("sortedBy");
                    element.find(".sortedDesc").removeClass("sortedDesc");
                    $(col).addClass("sortedBy");
                    if (desc) {
                        $(col).addClass("sortedDesc");
                    }
                    config.headerClickHandler(index);
                });
            })
      }
    };
});
