angular.module('infiniteScroll', []).directive('infiniteScroll', function($window, $q) {
  return {
	restrict: 'A',
	scope:{
		infiniteScroll: '&'
	},
    link: function(scope, element, attrs) {
		  var offset, scrolling;
		  offset = parseInt(attrs.infiniteScrollOffset, 10) || 10;
		  container = attrs.infiniteScrollContainer;
		  scrolling = false;
		  return angular.element(container ? $(container) : $window).bind('scroll', function() {
			var deferred, _ref;
			if (!scrolling && element[0].scrollTop + element[0].offsetHeight >= element[0].scrollHeight - offset) {
			  scrolling = true;
			  deferred = $q.defer();
			  scope.infiniteScroll({ deferredObj : deferred });
			  return deferred.promise.then(function() {
				return scrolling = false;
			  });
			}
		  });
    }
  };
});