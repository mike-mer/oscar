(function () {
  'use strict';

	angular.module("oscar").component('dictionaryTree', {
		bindings: {
			tree : '<treeData',
            contract: '=?',
			treeType : '@'
		},
		require: {
            parent: '^?nsdNew'
        },
		templateUrl: 'controls/dictionaryTree/dictionaryTree.html',
		controller: dictionaryTreeController
	}).service('nsdGalleryModel', dictionaryTreeModel);

	dictionaryTreeController.$inject = ['$http', '$rootScope', 'api'];
	function dictionaryTreeController($http, $rootScope, api) {
		var self = this;

		this.$onInit = function () {
			var treeType = this.treeType;
			self.tree = [];
		};
		this.$onChanges = function (changesObj) {
			if(changesObj.tree && changesObj.tree.currentValue != undefined) {
				self.tree = angular.copy(self.tree);
				self.contract.getTree = self.getStateCallbak;
			}
		};
		this.initCategoryWord = function(data, source) {
			if (data === true) return;
			data.CategoryWords.isOpen = true
		};
		this.getStateCallbak = function() {
			return self.tree;
		};

        this.tree = [];
        this.currentNode = {};
        this.nodeName = "";
        this.nameToAdd = "";
        this.isEdit = true;
        this.isAdd = false;
        this.selectedDictionary = undefined;
        this.currentObj = {
            currentNode: {},
            nodeName: "",
            indication: ""
        };

        var self = this;

		this.gotfile = function(ctrl) {
			self.loading = true;
			var path = ctrl.value;

			var data = new FormData();
			data.append('file', ctrl.files[0]);
			data.append('dictionaryName', self.dictionaryName);


			$http.post("/Dictionaries/CSV", data, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined, 'X-Force-Content-Type': 'text/plain' },
				responseType: 'text/plain',
				transformResponse: [function (data) {
				  return data;
			  }]
			}).then(function (response) {
				self.loading = false;
				var name = self.dictionaryName;
				self.dictionaryName = "";
				setTimeout(function() { $rootScope.showInfoMessage({title: "info", content: "File imported successfully", type: "info"}); }, 1);
				self.getDictionaryList(name);
			}, function (response) {
				self.loading = false;
				self.dictionaryName = "";
				$rootScope.showInfoMessage({title: "info", content: "Imported failed", type: "error"});
			});

		};

		this.MandatoryAtLeastChanged = function(dic) {
			if (!dic.MandatoryAtLeast) {
				delete dic.MandatoryAtLeast;
			}
		};

        this.add = function (toAnimate) {
            if (this.currentObj.indication == 'dictionary') {
                var newnode = {
                    CategoryName: this.nameToAdd,
                    CategoryWords: []
                };
                this.currentObj.currentNode.DictionaryCategory.push(newnode);

            }
            if (this.currentObj.indication == 'category') {
				var newnode = {
                    "title": this.nameToAdd,
                    "score": "1",
                    "exact": false,
                    "mandatory": false
                };
                this.currentObj.currentNode.CategoryWords.push(newnode);
            }

            this.nameToAdd = "";
			$("#editField").focus();

			if (toAnimate && false) {
				setTimeout( function() {
				var container = $('.nodesRoot'), scrollTo = $('.currentNode');
				container.animate({
					scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop() - 10
				}) }, 100);
			}
        };

        this.remove = function () {
            if (this.currentObj.indication == 'category') {
                this.tree.DictionaryCategory.splice(this.tree.DictionaryCategory.indexOf(this.currentObj.currentNode), 1)
            }
            if (this.currentObj.indication == 'dictionary') {
                this.tree.DictionaryCategory = [];
                this.tree.DictionaryName = "New Dictionary"
            }
            if (this.currentObj.indication == 'none') {
                this.currentObj.currentNode.CategoryWords.splice(this.currentObj.nodeName, 1);
            }
        };

		this.editFieldChanged = function() {
			if (this.currentObj.indication == 'category') {
                this.currentObj.currentNode.CategoryName = this.nodeName;
            }
            if (this.currentObj.indication == 'dictionary') {
                this.tree.DictionaryName = this.nodeName;
            }
            if (this.currentObj.indication == 'none') {
                this.currentObj.currentNode.CategoryWords[this.currentObj.nodeName].title = this.nodeName;
            }
		};

        this.edit = function () {
            if (this.currentObj.indication == 'category') {
                this.currentObj.currentNode.CategoryName = this.nodeName;
            }
            if (this.currentObj.indication == 'dictionary') {
                this.tree.DictionaryName = this.nodeName;
            }
            if (this.currentObj.indication == 'none') {
                this.currentObj.currentNode.CategoryWords[this.currentObj.nodeName].title = this.nodeName;
            }

            this.nodeName = "";
        };

        this.setCurrentNode = function (obj, nameToEdit, name) {
            if (name == 'none') {
                this.isEdit = true;
                this.isAdd = false;
            } else {
                this.isEdit = true;
                this.isAdd = true;
            }
            this.currentObj.currentNode = obj;
            this.currentObj.nodeName = nameToEdit;
            this.currentObj.indication = name;

			this.nodeName = this.currentObj.indication == "none" ? obj.CategoryWords[nameToEdit].title : this.currentObj.nodeName;

			$("#editField").focus();
        };
	}

	function dictionaryTreeModel() {
		  this.data = {};
	}

	dictionaryTreeModel.prototype.modelName = "dictionaryTreeModel";
})();
