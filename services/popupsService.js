(function() {

	angular.module("oscar").service('popups', popupsService);



	function popupsService(generalService) {
/* 	  this.addPopup = function(params) {
			return "ok";
	  }; */
	  
	  var self = this;
	  
	  
	  var subscribers = [];
	  var popups = [];
	  
	  
	  function createPopup() {
			var guid = generalService.helpers.getGuid();
			return {
				id: guid,
				visible : false,
				type : "info",
				title : "",
				width : '600px',
				height : '250px',
				height2 : '125px',
				buttons : [ {label : "close", width : '100px', isActive : function() { return true; }, callback : function() { self.removePopup(guid); }} ]
			}
		};
		
		
		function notifyAll(data) {
			subscribers.forEach(function(sub) {
				sub(data);
			})
		}
		
		
		this.subscribe = function(callback) {
			subscribers.push(callback);
		}
		
		
		
		this.removePopup = function(id) {
			//debugger;
			popups = popups.filter(function(p) { return p.id != id });
			notifyAll(popups);
		}
		
		
		function halfMeasure(data) {
			var digits = parseFloat(data);
			var points = data.split(digits)[1];
			var half = (digits / 2) + points;
			return half;
		}
		
		this.showPopup  = function(popup) {
			if (!popup) popup = {};
			var newPopup =  createPopup();
			//newPopup.id = guid;
			newPopup.type = popup.type || "info";
			newPopup.title = popup.title || "";
			if (popup.url) {
				newPopup.url = popup.url;
			}
			else {
				newPopup.content = popup.content || "";
			}
			newPopup.width = popup.width || '600px';
			newPopup.height = popup.height || '250px';
			newPopup.height2 = halfMeasure(newPopup.height);
			newPopup.buttons = popup.buttons || [ {label : "close", width : '100px', isActive : function() { return true; }, callback : function() { self.removePopup(newPopup.id); }} ];
			newPopup.visible = true;
			newPopup.close = function() { self.removePopup(newPopup.id); };
			
			popups.push(newPopup);
			notifyAll(popups);
			return newPopup;
		};
		
		
		this.closePopup = function(popup) {
			
		};


		this.showInfoMessage = function(params) {
			this.showPopup({
				title : params.title || "",
				content : params.content || "",
				type : params.type || "info",
				buttons : [
							{
								label : "Close",
								width : '100px',
								isActive : function() { return true; },
								callback : function() { $rootScope.systemPopup.visible = false }
							}
						]
			});
		}
		
	  
	  
	}

}());