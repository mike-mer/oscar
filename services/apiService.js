(function() {

	angular.module("oscar").factory('api', function(apiPoints) {
		return new ApiService(apiPoints);
	});


	//ApiService.$inject = ['apiPoints'];
	function ApiService(apiPoints) {
	  var version = "1.0.0";
	  //this.version = "";
	  var self = this;

	  this.runningRequests = 0;

	  this.execute = function(params) {
			var apiModule = params.module ? apiPoints[params.module] : apiPoints;
			var api = apiModule[params.method];

			if (!params.suppressGlobalLoader) {
					$(".page-loading-overlay").removeClass("loaded");
					self.runningRequests++;
			}
			console.log("LOAD",self.runningRequests,params);

			return api(params.data).then(function(data) {
				if (!params.suppressGlobalLoader) {
						self.runningRequests--;
						if (self.runningRequests == 0) {
							$(".page-loading-overlay").addClass("loaded");
						}
				}
							console.log("DATA",self.runningRequests,params);
				return data;
			});
	  };
	}

}());
