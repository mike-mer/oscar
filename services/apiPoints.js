(function() {

	angular.module("oscar").factory('apiPoints', function($http, appConfig) {
		return new apiPoints($http, appConfig);
	});



	function apiPoints($http, appConfig) {

	    config = {
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            }
        };


		this.taskManager = {


			getTasks: function(params) {

				var containerId = "2";
				var path = appConfig.host + '/RobotExecution/collectionQueue/list/queue/request/' + containerId + (params.type ? ("/" + params.type + "/" + params.size + "/" + params.page + "/" + (params.sortColumn || "collectionRequestId") + "/" + (params.sortOrder || "desc") ) : "");

				return $http.post(path)
					.then(function (data) {
						return data.data.data;
					},function (error, status){
						console.log(error);
						return null;
						//errorHandler($scope, status);
					});

			},



			getTasks2: function(params) {

				var containerId = "2";
				var path = appConfig.host + '/RobotExecution/collectionQueue/list/queue/request/' + containerId + (params.type ? ("/" + params.type + "/" + params.size + "/" + params.page) : "");

				return $http.post(path, null, {
					transformResponse: function (data, headersGetter, status) {
							//debugger;
							var data = data.data;
							data = data.map(function(d) {
								return [d.status, d.collectionRequestId, d.creationTime, d.lastDate, d.nextDate];
							});

							return data;
					}
				})
					.then(function (data) {
						return data.data.data;
					},function (error, status){
						console.log(error);
						return null;
						//errorHandler($scope, status);
					});

			}

		}
        this.profile = {
            getEntitySummary: function (params) { return $http({ url: appConfig.host + "/EntitiesDiscovery/summary/" + params.id, method: "GET" }); },
            getEntitySummaryId: function (params) { return $http({ url: appConfig.host + "/EntitiesDiscovery/summary", method: "POST", data: params.data }); },
            getEntitySummaryIdForce: function (params) { return $http({ url: appConfig.host + "/EntitiesDiscovery/summary?force=true", method: "POST", data: params.data }); },
            //getLocations: function (params) { return $http({ url: appConfig.host + "/NewSourcesDiscovery/locations/" + params.id, method: "GET" }); },
            //getLastNamesStats: function (params) { return $http({ dataType: "json", url: appConfig.host + "/NewSourcesDiscovery/lastNamesStats/" + params.id, method: "POST" }); },
            getAnalysisPerTimeResults: function (params) { return $http({ url: appConfig.host + "/EntitiesDiscovery/summary/statistics/" + params.id + "?timedStatistics=true", method: "GET" }); },
            getEntityStatisticsResults: function (params) { return $http({ url: appConfig.host + "/EntitiesDiscovery/summary/statistics/" + params.id, method: "GET" }); },
            getEntityStatisticsLinks: function (params) { return $http({ url: appConfig.host + "/EntitiesDiscovery/summary/statistics/" + params.id + "/links", method: "GET" }); },
            getEntityStatisticsContent: function (params) { return $http({ url: appConfig.host + "/EntitiesDiscovery/summary/" + params.id + "/content", method: "GET" }); },
            runFullLA: function (params) { return $http({ url: appConfig.host + "/EntitiesDiscovery/summary/" + params.id + "/closeLinksContent", method: "POST" }); },

						clearCache: function (params) { return $http({ url: appConfig.host + "/EntitiesDiscovery/summary/clearcache/" + params.id, method: "GET" }); },

            //getEntityStatisticsLinks: function (params) { return $http({ url: appConfig.host + "/EntitiesDiscovery/summary/statistics/" + params.id + "/links", method: "GET" }); },
            //getGetUserAboutDetails: function (params) { return $http({ url: appConfig.host + "/NewSourcesDiscovery/GetUserAboutDetails?uid=" + params.id, method: "POST" }); },
            //getFriends: function (params) { return $http({ url: appConfig.host + "/NewSourcesDiscovery/Friends?id=" + params.id, method: "POST" }); },
            //getProfilePictures: function (params) { return $http({ url: appConfig.host + "/NewSourcesDiscovery/profilePicture/" + params.id, method: "GET" }); },
        };




		this.CIFER = {
			test: function(params) {
				var path =   appConfig.host + '/NewSourcesDiscoveryServer/ScoringTest?d=' + params.dictionary;

				return $http({
					method: "POST",
					url: path,
					data: params.text
				}).then(function (data) {
						return data.data;
					},function (error, status){
						console.log(error);
						return null;
					});

			}

		},


		this.NSD = {

			getSearchSources: function(params) {
				var path =   appConfig.host + '/NewSourcesDiscoveryServer/NsdQueryServer';

				return $http.get(path)
					.then(function (data) {
						//debugger;
						return data.data;
					},function (error, status){
						console.log(error);
						return null;
						//errorHandler($scope, status);
					});
			},



			getDictionary: function(params) {
				var path =   appConfig.host + '/Dictionaries/Dictionary/' + params.name;

				return $http.get(path)
					.then(function (data) {
						//debugger;
						return data.data;
					},function (error, status){
						console.log(error);
						return null;
						//errorHandler($scope, status);
					});
			},



			getDictionaries: function(params) {
				var path =   appConfig.host + '/Dictionaries/DictionaryList';

				return $http.get(path)
					.then(function (data) {
						return data.data;
					},function (error, status){
						console.log(error);
						return null;
						//errorHandler($scope, status);
					});
			},


			runSearch: function(params) {
				var path =   appConfig.host + '/NewSourcesDiscoveryServer/NsdQueryServer';

				return $http.post(path, params.payload)
					.then(function (data) {
						return data.data;
					},function (error, status){
						console.log(error);
						return null;
					});
			},



			getQuery: function(params) {
				var path =   appConfig.host + '/NewSourcesDiscoveryServer/GetQuery/' + params.id;

				return $http.get(path)
					.then(function (data) {
						return data.data;
					},function (error, status){
						console.log(error);
						return null;
					});
			},




			getTableNSD: function(params) {
				var path = appConfig.host + "/NewSourcesDiscoveryServer/NsdResultsServer/"+ params.id +"?order=" + (params.order || "Website") + (params.asc ? "&asc=true" : "") + ("&pageSize=" + (params.size || 1000)) + ("&pageNumber=" + (params.index || 0));

				return $http.get(path)
					.then(function (data) {
						return data.data;
					},function (error, status){
						console.log(error);
						return null;
					});
			}


		}



		this.ED = {
			getList : function(params) {
				var path =   appConfig.host + '/EntitiesDiscovery/discovery/' + params.id

				return $http.get(path)
					.then(function (data) {
						return data.data;
					},function (error, status){
						return null;
					});
			},
			query : function(params) {
				var path =   appConfig.host + '/EntitiesDiscovery/summary/query/' + params.id

				return $http.get(path)
					.then(function (data) {
						return data.data;
					},function (error, status){
						return null;
					});
			},
			search: function(params) {
				var path =   appConfig.host + '/EntitiesDiscovery/discovery';

				return $http.post(path, params.payload)
					.then(function (data) {
						return data.data;
					},function (error, status){
						return null;
					});
			}

		}



		this.Avatars = {

			getAvatars : function(params) {

				var path =   appConfig.host + '/AvatarManager/GetAvatars';

				return $http.get(path)
					.then(function (data) {
						//debugger;
						return data.data;
					},function (error, status){
						console.log(error);
						return null;
						//errorHandler($scope, status);
					});


			}



		}






	}





}());
