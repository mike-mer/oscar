(function() {

	angular.module("oscar").service('popupsReduser', function () {
		
		  return function( state, action ) {
			switch (action.type) {
			  case 'ADD_POPUP':
				state.popupsModule.popupsList = state.popupsModule.popupsList.push( Immutable.Map( {title: '', content: ''} ) );
				return state;
			  case 'REMOVE_POPUP':
				state.popupsModule.popupsList = state.popupsModule.popupsList.splice(action.index, 1);
				return state;
/* 			case 'CONFIRM_ITEM':
				state.list = state.list.setIn([action.index, 'added'] ,true)
				return state;
			  case 'MODIFY_ITEM':
				state.list = state.list.setIn([action.index, 'value'], action.value)
				return state */
			  default:
				return state;
			}
		  }
	  
	});


}());