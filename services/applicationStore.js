
(function() {
	
	angular.module("oscar").service('Redux', function (){
		return Redux;
	});
	
	

	angular.module("oscar").factory('applicationStore', function(Redux, popupsReduser) {
		  var initialState = {
				popupsModule: {
					popupsList: Immutable.List([])
				}
		  };
		  return Redux.createStore(popupsReduser, initialState);
	});
	

}());