function SocialSetChart(data) {
    var Social;
    Social =Social ||  {};

    Social.assetDataPath = "inc/data/";
    Social.assetImgPath = "img/";
    Social.netChartDefaultValue = 'f-23';
    Social.netChartDataUrl = "graph.json";
    Social.pieChartMinValue = 50;
    Social.pieChartMaxValue = 150;
    Social.graphTopics = {
        nodes: {
            "m-11": {
                "Bar": 483.0,
                "Apple": 923.0,
                "Office": 391.0,
                "Money": 777.0,
                "Photography": 595.0,
                "Music": 533.0,
                "Sports": 109.0,
                "Running": 31.0,
                "Health": 295.0,
                "Marriage": 322.0,
                "Business": 93.0,
                "Todo": 709.0
            },
            "m-18": {
                "Sports": 109.0,
                "Running": 31.0,
                "Health": 295.0,
                "Marriage": 322.0,
                "Business": 93.0
            },
            "m-19": {
                "Running": 31.0,
                "Health": 295.0,
                "Marriage": 322.0,
                "Family": 172.0,
                "Money": 787.0,
                "Business": 93.0
            },
            "m-10": {
                "Television": 650.0,
                "Bar": 530.0,
                "Apple": 923.0,
                "Family": 172.0,
                "Money": 787.0,
                "Photography": 517.0,
                "Marriage": 621.0,
                "Vacation": 139.0,
                "Sports": 184.0,
                "Running": 413.0,
                "College": 467.0,
                "Office": 391.0,
                "Internet": 315.0,
                "Health": 171.0,
                "Todo": 53.0,
                "Clothing": 164.0
            },
            "m-34": {
                "Television": 650.0,
                "Bar": 930.0,
                "Apple": 923.0,
                "Family": 172.0,
                "Money": 787.0,
                "Photography": 517.0,
                "Marriage": 621.0,
                "Vacation": 139.0,
                "Sports": 184.0,
                "Running": 413.0,
                "College": 467.0,
                "Office": 391.0,
                "Internet": 315.0,
                "Clothing": 164.0
            },
            "m-37": {
                "Family": 172.0,
                "Money": 787.0,
                "Photography": 517.0,
                "Running": 413.0,
                "College": 467.0,
                "Office": 391.0,
                "Internet": 315.0,
                "Clothing": 164.0
            },
            "m-38": {
                "Family": 172.0,
                "Money": 587.0,
                "Car": 817.0,
                "Running": 113.0,
                "College": 267.0,
                "Office": 391.0,
                "Internet": 315.0,
                "Clothing": 664.0
            },
            "m-36": {
                "Family": 172.0,
                "Money": 187.0,
                "Vacation": 139.0,
                "Sports": 784.0,
                "Running": 913.0,
                "College": 467.0,
                "Office": 391.0
            },
            "m-35": {
                "Television": 650.0,
                "Bar": 930.0,
                "Apple": 923.0,
                "Internet": 315.0
            },
            "m-17": {
                "Bar": 530.0,
                "Apple": 923.0,
                "Family": 172.0,
                "Money": 787.0,
                "Photography": 517.0,
                "Marriage": 621.0,
                "Vacation": 139.0,
                "Sports": 184.0,
                "Running": 413.0,
                "College": 467.0,
                "Clothing": 164.0
            },
            "m-16": {
                "Television": 650.0,
                "Bar": 530.0,
                "Apple": 923.0,
                "Money": 787.0,
                "Photography": 517.0,
                "Marriage": 621.0,
                "Vacation": 139.0,
                "Sports": 184.0,
                "Running": 413.0,
                "College": 467.0,
                "Office": 391.0,
                "Internet": 315.0,
                "Health": 171.0,
                "Todo": 53.0,
                "Clothing": 164.0
            },
            "m-13": {
                "Shopping": 157.0,
                "Love": 656.0,
                "Weight": 82.0,
                "Business": 687.0,
                "Car": 397.0,
                "Photography": 455.0,
                "Marriage": 422.0,
                "College": 41.0,
                "Money": 304.0,
                "Baby": 236.0,
                "Health": 150.0,
                "Android": 752.0,
                "Family": 79.0
            },
            "m-15": {
                "Shopping": 157.0,
                "Love": 656.0,
                "Weight": 82.0,
                "Business": 687.0,
                "Car": 397.0,
                "Photography": 455.0,
                "Marriage": 422.0,
                "College": 41.0,
                "Money": 304.0,
                "Baby": 236.0,
                "Health": 150.0,
                "Android": 752.0,
                "Family": 79.0
            },
            "m-33": {
                "Weight": 82.0,
                "Business": 687.0,
                "Car": 397.0,
                "Photography": 455.0,
                "Marriage": 422.0,
                "College": 41.0,
                "Money": 304.0,
                "Baby": 236.0,
                "Health": 150.0,
                "Android": 752.0,
                "Family": 79.0
            },
            "m-25": {
                "Shopping": 857.0,
                "Weight": 82.0,
                "Car": 397.0,
                "Photography": 455.0,
                "Marriage": 422.0,
                "College": 41.0,
                "Money": 304.0,
                "Baby": 236.0,
                "Health": 150.0,
                "Android": 752.0,
                "Family": 79.0
            },
            "m-26": {
                "Weight": 82.0,
                "Car": 997.0,
                "Photography": 455.0,
                "College": 41.0,
                "Money": 304.0,
                "Baby": 236.0,
                "Android": 252.0,
                "Family": 79.0
            },
            "m-27": {
                "Weight": 82.0,
                "Car": 97.0,
                "Photography": 55.0,
                "College": 41.0,
                "Money": 904.0,
                "Android": 752.0,
                "Family": 79.0
            },
            "m-28": {
                "Weight": 82.0,
                "Car": 597.0,
                "Photography": 55.0,
                "College": 241.0,
                "Android": 952.0,
                "Family": 79.0
            },
            "m-21": {
                "Photography": 455.0,
                "Marriage": 422.0,
                "College": 41.0,
                "Money": 304.0,
                "Baby": 236.0,
                "Health": 150.0,
                "Android": 752.0,
                "Family": 79.0
            },
            "m-29": {
                "Car": 422.0,
                "College": 41.0,
                "Money": 304.0,
                "Baby": 236.0,
                "Health": 150.0,
                "Android": 752.0,
                "Family": 79.0
            },
            "m-22": {
                "Marriage": 422.0,
                "College": 41.0,
                "Money": 304.0,
                "Baby": 236.0,
                "Health": 150.0,
                "Family": 79.0
            },
            "m-12": {
                "Food": 227.0,
                "Todo": 326.0,
                "Android": 752.0,
                "Health": 295.0,
                "Weight": 82.0
            },
            "m-20": {
                "Food": 227.0,
                "Todo": 326.0,
                "Business": 687.0,
                "Android": 352.0,
                "Health": 295.0,
                "Love": 82.0
            },
            "m-31": {
                "Car": 227.0,
                "Todo": 326.0,
                "Business": 687.0,
                "Android": 352.0,
                "Health": 295.0,
                "Love": 82.0
            },
            "m-14": {
                "Food": 227.0,
                "Todo": 326.0,
                "Android": 752.0,
                "Health": 295.0,
                "Weight": 82.0
            },
            "m-32": {
                "Food": 227.0,
                "Todo": 326.0,
                "Android": 152.0,
                "Health": 495.0,
                "Car": 82.0
            },
            "m-23": {
                "Food": 227.0,
                "Todo": 326.0,
                "Money": 752.0,
                "Health": 295.0,
                "College": 82.0
            },
            "m-24": {
                "Food": 227.0,
                "Todo": 326.0,
                "Money": 75.0,
                "Baby": 236.0,
                "Health": 150.0,
                "College": 482.0
            },
            "m-30": {
                "Car": 627.0,
                "Todo": 326.0,
                "Money": 75.0,
                "Baby": 236.0,
                "Health": 150.0,
                "College": 182.0
            },
            "f-8": {
                "Love": 647.0,
                "Cinema": 226.0,
                "Money": 493.0,
                "Photography": 292.0,
                "Weather": 437.0,
                "Books": 409.0,
                "Marriage": 845.0
            },
            "f-34": {
                "Cinema": 226.0,
                "Money": 493.0,
                "Photography": 292.0,
                "Weather": 437.0,
                "Books": 409.0
            },
            "f-9": {
                "Apple": 59.0,
                "Health": 478.0,
                "Cinema": 380.0,
                "Food": 760.0,
                "Money": 53.0,
                "Books": 91.0,
                "Marriage": 117.0,
                "Home": 144.0,
                "Clothing": 100.0
            },
            "f-35": {
                "Apple": 59.0,
                "Cinema": 380.0,
                "Food": 760.0,
                "Money": 53.0,
                "Books": 91.0,
                "Marriage": 117.0,
                "Home": 144.0,
                "Clothing": 100.0
            },
            "f-6": {
                "Bar": 604.0,
                "Office": 19.0,
                "Cinema": 234.0,
                "College": 330.0,
                "Photography": 8.0,
                "Cycling": 393.0,
                "Art": 165.0,
                "Running": 10.0,
                "Music": 307.0,
                "Money": 475.0,
                "Home": 818.0,
                "Women": 16.0
            },
            "f-36": {
                "Office": 19.0,
                "Cinema": 234.0,
                "College": 330.0,
                "Photography": 8.0,
                "Cycling": 393.0,
                "Art": 165.0,
                "Running": 10.0,
                "Music": 307.0,
                "Money": 475.0,
                "Home": 818.0,
                "Women": 16.0
            },
            "f-7": {
                "Bar": 325.0,
                "Marriage": 539.0,
                "Art": 98.0,
                "Weight": 133.0
            },
            "f-37": {
                "Bar": 325.0,
                "Marriage": 539.0,
                "Art": 98.0,
                "Photography": 888.0,
                "Weight": 133.0
            },
            "f-4": {
                "Map": 115.0,
                "Television": 207.0,
                "Art": 678.0,
                "Office": 280.0,
                "Family": 544.0,
                "Cinema": 797.0,
                "Car": 209.0,
                "Photography": 275.0,
                "Vacation": 399.0,
                "Weight": 112.0,
                "Running": 54.0,
                "Books": 381.0,
                "Marriage": 1856.0,
                "Home": 331.0,
                "Business": 297.0
            },
            "f-38": {
                "Map": 115.0,
                "Television": 207.0,
                "Art": 678.0,
                "Office": 280.0,
                "Cinema": 797.0,
                "Car": 209.0,
                "Photography": 275.0,
                "Vacation": 399.0,
                "Weight": 112.0,
                "Running": 54.0,
                "Books": 381.0,
                "Home": 331.0,
                "Business": 297.0
            },
            "f-5": {
                "Television": 424.0,
                "Bar": 530.0,
                "Business": 344.0,
                "Food": 242.0,
                "Car": 176.0,
                "Photography": 824.0,
                "Running": 22.0,
                "Books": 146.0,
                "Money": 698.0,
                "Art": 228.0,
                "Clothing": 34.0
            },
            "f-39": {
                "Television": 424.0,
                "Bar": 530.0,
                "Business": 1344.0,
                "Food": 242.0,
                "Car": 176.0,
                "Running": 22.0,
                "Books": 146.0,
                "Money": 998.0,
                "Art": 228.0,
                "Clothing": 34.0
            },
            "f-2": {
                "Shopping": 215.0,
                "Photography": 392.0,
                "Books": 70.0,
                "Health": 345.0,
                "Weather": 340.0
            },
            "f-40": {
                "Shopping": 215.0,
                "Photography": 392.0,
                "Books": 70.0,
                "Health": 345.0,
                "Weather": 340.0
            },
            "f-3": {
                "Art": 474.0,
                "Weather": 540.0,
                "Marriage": 843.0,
                "Yoga": 93.0,
                "Women": 62.0
            },
            "f-41": {
                "Art": 1474.0,
                "Vacation": 309.0,
                "Weather": 540.0,
                "Marriage": 843.0,
                "Yoga": 93.0,
                "Women": 62.0
            },
            "f-1": {
                "Car": 312.0,
                "Television": 281.0,
                "Books": 574.0
            },
            "f-42": {
                "Car": 312.0,
                "Television": 881.0,
                "Art": 174.0,
                "Books": 574.0
            },
            "f-10": {
                "Shopping": 672.0,
                "Art": 64.0,
                "Apple": 60.0,
                "Family": 124.0,
                "Car": 53.0,
                "Cooking": 177.0,
                "Vacation": 242.0,
                "Cycling": 155.0,
                "Music": 99.0,
                "Office": 179.0,
                "Home": 537.0,
                "Android": 190.0,
                "Clothing": 737.0
            },
            "f-43": {
                "Shopping": 1672.0,
                "Art": 64.0,
                "Apple": 260.0,
                "Family": 124.0,
                "Home": 537.0,
                "Clothing": 737.0
            },
            "f-11": {
                "Map": 5.0,
                "Art": 24.0,
                "Yoga": 842.0,
                "Food": 706.0,
                "Money": 163.0,
                "Sports": 71.0,
                "Crush": 897.0,
                "Health": 107.0,
                "Fishing": 63.0,
                "Love": 188.0,
                "Home": 1026.0
            },
            "f-44": {
                "Map": 5.0,
                "Art": 24.0,
                "Yoga": 842.0,
                "Money": 163.0,
                "Sports": 71.0,
                "Crush": 897.0,
                "Health": 107.0,
                "Home": 1026.0
            },
            "f-12": {
                "Shopping": 157.0,
                "Love": 975.0,
                "Family": 79.0,
                "College": 41.0,
                "Vacation": 394.0,
                "Weather": 626.0,
                "Running": 88.0,
                "Experiment": 220.0,
                "Health": 339.0,
                "Internet": 283.0,
                "Baby": 236.0,
                "Music": 98.0
            },
            "f-45": {
                "Shopping": 157.0,
                "Love": 975.0,
                "Family": 79.0,
                "College": 41.0,
                "Vacation": 394.0,
                "Running": 588.0,
                "Experiment": 220.0,
                "Health": 339.0,
                "Internet": 183.0,
                "Baby": 236.0,
                "Music": 98.0
            },
            "f-13": {
                "Television": 134.0,
                "Art": 739.0,
                "Family": 531.0,
                "Cinema": 999.0,
                "Money": 789.0,
                "Photography": 130.0,
                "Running": 31.0,
                "College": 327.0,
                "Yoga": 241.0,
                "Todo": 383.0
            },
            "f-64": {
                "Television": 134.0,
                "Art": 139.0,
                "Family": 531.0,
                "Cinema": 499.0,
                "Money": 989.0,
                "Yoga": 241.0,
                "Todo": 383.0
            },
            "f-46": {
                "Television": 134.0,
                "Art": 739.0,
                "Cinema": 999.0,
                "Money": 789.0,
                "Photography": 130.0,
                "Running": 31.0,
                "Yoga": 241.0,
                "Todo": 383.0
            },
            "f-14": {
                "Money": 459.0,
                "Bar": 599.0,
                "Apple": 444.0
            },
            "f-47": {
                "Money": 459.0,
                "Bar": 599.0,
                "Family": 531.0,
                "Apple": 144.0
            },
            "f-15": {
                "Television": 326.0,
                "Yoga": 842.0,
                "Office": 178.0,
                "Business": 27.0,
                "Health": 94.0,
                "Food": 356.0,
                "Car": 169.0,
                "Photography": 257.0,
                "Money": 272.0,
                "Sports": 166.0,
                "Crush": 796.0,
                "Running": 508.0,
                "Weather": 322.0,
                "Marriage": 919.0,
                "Fishing": 63.0,
                "Internet": 1.0,
                "College": 456.0
            },
            "f-63": {
                "Photography": 257.0,
                "Money": 272.0,
                "Sports": 166.0,
                "Crush": 496.0,
                "Running": 808.0,
                "Weather": 622.0,
                "Marriage": 419.0,
                "Fishing": 63.0,
                "Internet": 1.0,
                "College": 456.0
            },
            "f-48": {
                "Health": 94.0,
                "Food": 356.0,
                "Car": 169.0,
                "Photography": 257.0,
                "Money": 272.0,
                "Sports": 166.0,
                "Crush": 796.0,
                "Weather": 322.0,
                "Marriage": 919.0,
                "Fishing": 63.0,
                "Internet": 1.0,
                "College": 456.0
            },
            "f-16": {
                "Love": 354.0,
                "Weight": 992.0,
                "Food": 463.0,
                "Music": 604.0,
                "Crush": 388.0,
                "Health": 110.0,
                "Home": 83.0,
                "Yoga": 800.0
            },
            "f-49": {
                "Love": 354.0,
                "Weight": 192.0,
                "Music": 604.0,
                "Crush": 388.0,
                "Health": 110.0,
                "Home": 83.0,
                "Yoga": 800.0
            },
            "f-17": {
                "Television": 61.0,
                "Love": 1062.0,
                "Apple": 59.0,
                "Weight": 568.0,
                "Cinema": 380.0,
                "Food": 760.0,
                "Photography": 195.0,
                "Health": 588.0,
                "Home": 59.0,
                "Music": 465.0,
                "Yoga": 800.0,
                "Todo": 19.0
            },
            "f-62": {
                "Television": 61.0,
                "Love": 362.0,
                "Apple": 59.0,
                "Weight": 568.0,
                "Cinema": 380.0,
                "Photography": 195.0,
                "Home": 59.0,
                "Music": 465.0,
                "Yoga": 800.0,
                "Todo": 19.0
            },
            "f-50": {
                "Television": 61.0,
                "Love": 1062.0,
                "Apple": 59.0,
                "Weight": 568.0,
                "Cinema": 380.0,
                "Home": 59.0,
                "Music": 465.0,
                "Yoga": 800.0,
                "Todo": 19.0
            },
            "f-18": {
                "Television": 385.0,
                "Love": 890.0,
                "Weight": 530.0,
                "Health": 171.0,
                "Food": 771.0,
                "Todo": 19.0,
                "Photography": 515.0,
                "Running": 244.0,
                "Books": 7.0,
                "Marriage": 303.0,
                "Experiment": 103.0,
                "College": 467.0,
                "Yoga": 262.0,
                "Clothing": 496.0,
                "Women": 67.0
            },
            "f-51": {
                "Television": 385.0,
                "Weight": 530.0,
                "Health": 171.0,
                "Food": 771.0,
                "Todo": 19.0,
                "Photography": 115.0,
                "Running": 244.0,
                "Books": 7.0,
                "Marriage": 803.0,
                "Experiment": 103.0,
                "College": 467.0,
                "Yoga": 262.0,
                "Clothing": 496.0,
                "Women": 67.0
            },
            "f-19": {
                "Weight": 819.0,
                "Food": 43.0,
                "Vacation": 216.0,
                "Weather": 248.0,
                "Marriage": 303.0,
                "Baby": 83.0,
                "Books": 7.0,
                "Clothing": 1035.0,
                "Women": 67.0
            },
            "f-52": {
                "Weight": 219.0,
                "Food": 43.0,
                "Vacation": 616.0,
                "Weather": 748.0,
                "Marriage": 303.0,
                "Baby": 83.0,
                "Books": 7.0,
                "Women": 67.0
            },
            "f-61": {
                "Weight": 219.0,
                "Food": 43.0,
                "Vacation": 616.0,
                "Marriage": 303.0,
                "Baby": 83.0,
                "Books": 7.0,
                "Women": 67.0
            },
            "f-32": {
                "Art": 478.0,
                "Office": 165.0,
                "Weight": 1580.0,
                "Men": 840.0,
                "Crush": 569.0,
                "Marriage": 57.0,
                "Family": 34.0
            },
            "f-53": {
                "Art": 478.0,
                "Office": 165.0,
                "Men": 840.0,
                "Crush": 569.0,
                "Marriage": 257.0,
                "Family": 34.0
            },
            "f-33": {
                "Shopping": 360.0,
                "Yoga": 274.0,
                "Office": 148.0,
                "Marriage": 418.0,
                "Crush": 1189.0,
                "Art": 175.0,
                "Music": 57.0,
                "Love": 355.0
            },
            "f-54": {
                "Shopping": 960.0,
                "Yoga": 274.0,
                "Office": 148.0,
                "Marriage": 418.0,
                "Art": 175.0,
                "Music": 57.0,
                "Love": 355.0
            },
            "f-30": {
                "Love": 182.0,
                "Weight": 481.0,
                "Food": 1191.0,
                "Vacation": 216.0,
                "Crush": 388.0,
                "Experiment": 103.0,
                "Music": 139.0,
                "Baby": 83.0,
                "Home": 83.0,
                "Yoga": 262.0
            },
            "f-55": {
                "Love": 182.0,
                "Weight": 481.0,
                "Experiment": 103.0,
                "Music": 139.0,
                "Baby": 83.0,
                "Home": 83.0,
                "Yoga": 262.0
            },
            "f-31": {
                "Television": 236.0,
                "Shopping": 608.0,
                "Apple": 274.0,
                "Business": 19.0,
                "Crush": 1088.0,
                "Music": 14.0,
                "Baby": 812.0,
                "Family": 426.0,
                "Todo": 94.0
            },
            "f-59": {
                "Television": 236.0,
                "Apple": 274.0,
                "Business": 19.0,
                "Crush": 488.0,
                "Music": 14.0,
                "Baby": 812.0,
                "Family": 426.0,
                "Todo": 94.0
            },
            "f-56": {
                "Television": 236.0,
                "Apple": 274.0,
                "Business": 19.0,
                "Crush": 1088.0,
                "Music": 514.0,
                "Family": 426.0,
                "Todo": 94.0
            },
            "m-9": {
                "Map": 262.0,
                "Television": 249.0,
                "Cycling": 538.0,
                "Apple": 276.0,
                "Business": 758.0,
                "Money": 309.0,
                "Photography": 688.0,
                "Vacation": 292.0,
                "Sports": 466.0,
                "Art": 32.0,
                "Weather": 76.0,
                "Family": 172.0,
                "Fishing": 58.0,
                "Internet": 315.0,
                "Bar": 47.0,
                "Android": 691.0,
                "Todo": 53.0
            },
            "f-57": {
                "Map": 262.0,
                "Television": 249.0,
                "Cycling": 538.0,
                "Apple": 276.0,
                "Business": 758.0,
                "Money": 309.0,
                "Vacation": 292.0,
                "Sports": 466.0,
                "Art": 32.0,
                "Weather": 76.0,
                "Family": 172.0,
                "Fishing": 58.0,
                "Internet": 315.0,
                "Bar": 47.0,
                "Todo": 53.0
            },
            "m-8": {
                "Gaming": 76.0,
                "Bar": 13.0,
                "Apple": 720.0,
                "Business": 723.0,
                "Money": 49.0,
                "Vacation": 153.0,
                "Cycling": 493.0,
                "Sports": 571.0,
                "Books": 259.0,
                "Fishing": 420.0,
                "Art": 223.0,
                "Android": 706.0,
                "Women": 809.0
            },
            "f-58": {
                "Gaming": 76.0,
                "Bar": 13.0,
                "Apple": 720.0,
                "Business": 723.0,
                "Money": 49.0,
                "Vacation": 153.0,
                "Cycling": 493.0,
                "Sports": 571.0,
                "Books": 259.0,
                "Fishing": 420.0,
                "Art": 223.0,
                "Women": 809.0
            },
            "m-5": {
                "Love": 1349.0,
                "Weight": 112.0,
                "Business": 840.0,
                "Cinema": 226.0,
                "Food": 350.0,
                "Car": 289.0,
                "Photography": 306.0,
                "Vacation": 399.0,
                "Sports": 38.0,
                "Health": 107.0,
                "Art": 24.0,
                "Running": 393.0,
                "Weather": 437.0,
                "Marriage": 298.0,
                "Office": 83.0,
                "Money": 163.0,
                "Baby": 140.0,
                "Home": 810.0,
                "Women": 71.0
            },
            "m-4": {
                "Map": 5.0,
                "Television": 447.0,
                "College": 123.0,
                "Bar": 599.0,
                "Family": 455.0,
                "Office": 178.0,
                "Car": 169.0,
                "Photography": 392.0,
                "Marriage": 800.0,
                "Vacation": 771.0,
                "Weather": 340.0,
                "Crush": 101.0,
                "Shopping": 215.0,
                "Books": 70.0,
                "Health": 534.0,
                "Money": 459.0,
                "Love": 547.0,
                "Art": 739.0,
                "Home": 547.0
            },
            "m-7": {
                "Cycling": 45.0,
                "Sports": 163.0,
                "Health": 94.0,
                "Internet": 1.0,
                "Yoga": 241.0,
                "Android": 292.0,
                "Gaming": 113.0,
                "Business": 226.0,
                "Photography": 449.0,
                "Women": 809.0,
                "Television": 134.0,
                "Bar": 13.0,
                "Car": 976.0,
                "Marriage": 1405.0,
                "Art": 223.0,
                "Love": 574.0,
                "Family": 531.0,
                "Cinema": 707.0,
                "Money": 811.0,
                "Weather": 322.0,
                "College": 783.0,
                "Fishing": 371.0,
                "Clothing": 271.0
            },
            "m-6": {
                "Love": 286.0,
                "Office": 83.0,
                "Cinema": 292.0,
                "Car": 976.0,
                "Marriage": 397.0,
                "Baby": 140.0,
                "Women": 71.0
            },
            "m-1": {
                "Television": 488.0,
                "Art": 1152.0,
                "Office": 19.0,
                "Business": 344.0,
                "Cinema": 234.0,
                "Food": 242.0,
                "Car": 488.0,
                "Photography": 367.0,
                "Vacation": 309.0,
                "Music": 307.0,
                "Weather": 540.0,
                "Bar": 604.0,
                "Books": 694.0,
                "Marriage": 1799.0,
                "Money": 242.0,
                "Yoga": 93.0,
                "Home": 432.0,
                "Clothing": 34.0,
                "Women": 78.0
            },
            "m-3": {
                "Map": 115.0,
                "Television": 404.0,
                "Art": 228.0,
                "Apple": 60.0,
                "Family": 668.0,
                "Cinema": 797.0,
                "Car": 53.0,
                "Photography": 261.0,
                "Marriage": 1862.0,
                "Vacation": 242.0,
                "Books": 648.0,
                "Music": 99.0,
                "Office": 280.0,
                "Money": 546.0,
                "Home": 622.0,
                "Android": 190.0,
                "Clothing": 350.0
            },
            "m-2": {
                "Television": 20.0,
                "Bar": 855.0,
                "Weight": 133.0,
                "Internet": 283.0,
                "Money": 931.0,
                "Photography": 465.0,
                "Marriage": 539.0,
                "Cycling": 393.0,
                "Weather": 626.0,
                "Music": 98.0,
                "Running": 120.0,
                "Experiment": 220.0,
                "College": 330.0,
                "Home": 386.0,
                "Art": 263.0
            },
            "f-29": {
                "Map": 1.0,
                "Television": 236.0,
                "Shopping": 1132.0,
                "Cycling": 190.0,
                "Office": 492.0,
                "Weight": 1292.0,
                "Car": 197.0,
                "Photography": 465.0,
                "Cooking": 319.0,
                "Crush": 1639.0,
                "Art": 64.0,
                "Marriage": 379.0,
                "Music": 604.0,
                "Todo": 94.0,
                "Baby": 61.0,
                "Love": 756.0,
                "Yoga": 398.0,
                "Clothing": 487.0,
                "Business": 93.0,
                "Family": 34.0
            },
            "f-28": {
                "Art": 393.0,
                "Weight": 882.0,
                "Food": 122.0,
                "Men": 766.0,
                "Cycling": 23.0,
                "Books": 629.0,
                "Music": 200.0,
                "Baby": 770.0,
                "Shopping": 960.0
            },
            "f-21": {
                "Television": 448.0,
                "Bar": 345.0,
                "Weight": 175.0,
                "Food": 548.0,
                "Photography": 328.0,
                "Men": 210.0,
                "Running": 227.0,
                "Books": 74.0
            },
            "f-20": {
                "Running": 227.0,
                "Books": 74.0,
                "Clothing": 703.0,
                "Weight": 346.0,
                "Weather": 248.0
            },
            "f-23": {
                "Television": 884.0,
                "Bar": 333.0,
                "Apple": 156.0,
                "Weight": 142.0,
                "College": 123.0,
                "Photography": 48.0,
                "Men": 341.0,
                "Vacation": 377.0,
                "Marriage": 1324.0,
                "Yoga": 194.0,
                "Android": 181.0,
                "Family": 455.0
            },
            "f-22": {
                "Television": 448.0,
                "Bar": 345.0,
                "Weight": 175.0,
                "Food": 321.0,
                "Photography": 328.0,
                "Men": 210.0,
                "Marriage": 524.0,
                "Android": 181.0
            },
            "f-25": {
                "Gaming": 37.0,
                "Television": 249.0,
                "Shopping": 132.0,
                "Bar": 302.0,
                "Business": 171.0,
                "Weight": 196.0,
                "Car": 317.0,
                "Photography": 455.0,
                "Vacation": 12.0,
                "Sports": 92.0,
                "Books": 550.0,
                "Marriage": 1268.0,
                "Money": 304.0,
                "Love": 574.0,
                "Android": 277.0,
                "Women": 11.0
            },
            "f-24": {
                "Love": 355.0,
                "Television": 437.0,
                "Shopping": 968.0,
                "Apple": 430.0,
                "Family": 426.0,
                "Bar": 333.0,
                "Photography": 48.0,
                "Men": 1181.0,
                "Yoga": 194.0,
                "Crush": 1207.0,
                "Weight": 962.0,
                "Marriage": 418.0,
                "Baby": 800.0,
                "Art": 653.0,
                "Business": 19.0
            },
            "f-27": {
                "Map": 1.0,
                "Shopping": 460.0,
                "Cycling": 58.0,
                "Weight": 1414.0,
                "Car": 197.0,
                "Cooking": 142.0,
                "Baby": 49.0,
                "Love": 756.0,
                "Yoga": 124.0
            },
            "f-26": {
                "Map": 262.0,
                "Shopping": 1092.0,
                "Art": 425.0,
                "Weight": 196.0,
                "Business": 261.0,
                "Food": 122.0,
                "Photography": 683.0,
                "Men": 766.0,
                "Vacation": 12.0,
                "Music": 200.0,
                "Sports": 92.0,
                "Weather": 76.0,
                "Marriage": 162.0,
                "Fishing": 9.0,
                "Baby": 770.0,
                "Bar": 302.0,
                "Books": 1179.0,
                "Clothing": 271.0,
                "Women": 11.0
            }
        },
        links: {
            "l144": {
                "Books": 259.0
            },
            "l142": {
                "Car": 317.0,
                "Photography": 455.0,
                "Marriage": 422.0,
                "Business": 171.0,
                "Money": 304.0
            },
            "l36": {
                "Car": 80.0,
                "Love": 228.0,
                "Business": 516.0
            },
            "l34": {
                "Todo": 326.0,
                "Health": 295.0
            },
            "l35": {
                "Android": 752.0,
                "Weight": 82.0
            },
            "l32": {
                "Bar": 47.0,
                "Family": 172.0,
                "Money": 309.0,
                "Photography": 197.0,
                "Vacation": 139.0,
                "Internet": 315.0,
                "Todo": 53.0
            },
            "l33": {
                "Money": 478.0,
                "Bar": 483.0,
                "Apple": 923.0,
                "Office": 391.0,
                "Sports": 109.0
            },
            "l30": {
                "Gaming": 76.0,
                "Bar": 13.0,
                "Business": 226.0,
                "Money": 49.0,
                "Sports": 105.0,
                "Fishing": 362.0,
                "Art": 223.0,
                "Android": 292.0,
                "Women": 809.0
            },
            "l31": {
                "Cycling": 493.0,
                "Apple": 276.0,
                "Business": 497.0,
                "Vacation": 153.0,
                "Sports": 466.0,
                "Fishing": 58.0,
                "Android": 414.0
            },
            "l123a": {
                "Television": 324.0,
                "Photography": 320.0,
                "Running": 244.0,
                "College": 467.0,
                "Health": 171.0,
                "Clothing": 164.0
            },
            "l123": {
                "Todo": 19.0,
                "Photography": 195.0,
                "Love": 708.0,
                "Television": 61.0
            },
            "l18": {
                "Money": 459.0,
                "Bar": 599.0
            },
            "l19": {
                "Art": 739.0
            },
            "l122": {
                "Weight": 473.0,
                "Food": 43.0,
                "Books": 7.0,
                "Marriage": 303.0,
                "Clothing": 332.0,
                "Women": 67.0
            },
            "l14": {
                "Money": 493.0,
                "Photography": 261.0,
                "Books": 409.0,
                "Marriage": 845.0
            },
            "l15": {
                "Map": 115.0,
                "Family": 544.0,
                "Office": 280.0,
                "Cinema": 797.0,
                "Books": 2.0,
                "Marriage": 900.0
            },
            "l16": {
                "Home": 85.0,
                "Marriage": 117.0,
                "Books": 91.0,
                "Clothing": 100.0,
                "Money": 53.0
            },
            "l17": {
                "Car": 169.0,
                "Office": 178.0
            },
            "l10": {
                "Bar": 325.0,
                "Marriage": 539.0,
                "Art": 98.0,
                "Weight": 133.0
            },
            "l11": {
                "Running": 88.0,
                "Experiment": 220.0,
                "Music": 98.0,
                "Weather": 626.0,
                "Internet": 283.0
            },
            "l12": {
                "Apple": 60.0,
                "Family": 124.0,
                "Car": 53.0,
                "Vacation": 242.0,
                "Music": 99.0,
                "Home": 537.0,
                "Android": 190.0,
                "Clothing": 250.0
            },
            "l13": {
                "Television": 404.0,
                "Books": 146.0,
                "Art": 228.0
            },
            "l130": {
                "Running": 169.0,
                "Marriage": 621.0,
                "Television": 326.0,
                "Sports": 75.0
            },
            "l131": {
                "Marriage": 800.0,
                "Television": 447.0,
                "College": 123.0,
                "Vacation": 377.0,
                "Family": 455.0
            },
            "l132": {
                "Money": 272.0,
                "Photography": 257.0,
                "Sports": 58.0,
                "Weather": 322.0,
                "Health": 94.0,
                "Internet": 1.0,
                "College": 456.0
            },
            "l133": {
                "Love": 428.0,
                "Shopping": 157.0,
                "Family": 79.0,
                "Health": 150.0,
                "Baby": 236.0,
                "College": 41.0
            },
            "l134": {
                "Food": 227.0
            },
            "l135": {
                "Music": 533.0,
                "Photography": 465.0,
                "Marriage": 322.0,
                "Business": 93.0
            },
            "l118": {
                "Android": 181.0,
                "Marriage": 524.0
            },
            "l119": {
                "Television": 448.0,
                "Bar": 345.0,
                "Weight": 175.0,
                "Food": 321.0,
                "Photography": 328.0,
                "Men": 210.0
            },
            "l116": {
                "Crush": 686.0,
                "Love": 355.0,
                "Shopping": 360.0,
                "Marriage": 418.0,
                "Art": 175.0
            },
            "l117": {
                "Television": 437.0,
                "Bar": 333.0,
                "Apple": 156.0,
                "Weight": 142.0,
                "Photography": 48.0,
                "Men": 341.0,
                "Yoga": 194.0
            },
            "l114": {
                "Shopping": 608.0,
                "Apple": 274.0,
                "Family": 426.0,
                "Crush": 521.0,
                "Baby": 800.0,
                "Business": 19.0
            },
            "l115": {
                "Men": 840.0,
                "Art": 478.0,
                "Weight": 820.0
            },
            "l112": {
                "Crush": 569.0,
                "Weight": 760.0,
                "Marriage": 57.0,
                "Office": 165.0,
                "Family": 34.0
            },
            "l113": {
                "Baby": 12.0,
                "Crush": 567.0,
                "Television": 236.0,
                "Music": 14.0,
                "Todo": 94.0
            },
            "l110": {
                "Shopping": 672.0,
                "Office": 179.0,
                "Cooking": 177.0,
                "Cycling": 155.0,
                "Art": 64.0,
                "Clothing": 487.0
            },
            "l111": {
                "Crush": 503.0,
                "Yoga": 274.0,
                "Music": 57.0,
                "Office": 148.0
            },
            "l136": {
                "Money": 299.0,
                "Photography": 130.0,
                "Todo": 383.0,
                "Running": 31.0
            },
            "l137": {
                "Television": 134.0,
                "Yoga": 241.0,
                "Family": 531.0,
                "Cinema": 707.0,
                "Money": 490.0,
                "College": 327.0
            },
            "l145": {
                "Food": 728.0,
                "Yoga": 262.0,
                "Experiment": 103.0,
                "Love": 182.0,
                "Weight": 57.0
            },
            "l21": {
                "Crush": 101.0,
                "Home": 547.0,
                "Map": 5.0
            },
            "l20": {
                "Love": 547.0,
                "Health": 189.0,
                "Vacation": 394.0
            },
            "l23": {
                "Weight": 112.0,
                "Business": 297.0,
                "Car": 209.0,
                "Photography": 275.0,
                "Vacation": 399.0,
                "Running": 54.0,
                "Home": 331.0
            },
            "l22": {
                "Running": 339.0,
                "Marriage": 298.0,
                "Business": 27.0
            },
            "l25": {
                "Art": 24.0,
                "Food": 350.0,
                "Money": 163.0,
                "Sports": 38.0,
                "Health": 107.0,
                "Home": 479.0,
                "Love": 188.0
            },
            "l24": {
                "Photography": 31.0,
                "Weather": 437.0,
                "Love": 647.0,
                "Cinema": 226.0
            },
            "l27": {
                "Crush": 796.0,
                "Food": 356.0,
                "Yoga": 842.0,
                "Fishing": 63.0,
                "Sports": 33.0
            },
            "l139": {
                "Cinema": 292.0
            },
            "l29": {
                "Car": 976.0,
                "Marriage": 397.0
            },
            "l28": {
                "Baby": 140.0,
                "Love": 286.0,
                "Office": 83.0,
                "Women": 71.0
            },
            "l140": {
                "Food": 760.0,
                "Home": 59.0,
                "Health": 478.0,
                "Apple": 59.0,
                "Cinema": 380.0
            },
            "l09": {
                "Art": 165.0,
                "Money": 475.0,
                "Running": 10.0,
                "College": 330.0,
                "Home": 386.0,
                "Cycling": 393.0
            },
            "l08": {
                "Television": 20.0,
                "Money": 456.0,
                "Photography": 465.0,
                "Bar": 530.0,
                "Running": 22.0
            },
            "l06": {
                "Bar": 604.0,
                "Office": 19.0,
                "Cinema": 234.0,
                "Photography": 8.0,
                "Music": 307.0,
                "Home": 432.0,
                "Women": 16.0
            },
            "l05": {
                "Business": 344.0,
                "Food": 242.0,
                "Car": 176.0,
                "Photography": 359.0,
                "Money": 242.0,
                "Clothing": 34.0
            },
            "l04": {
                "Marriage": 956.0,
                "Television": 207.0,
                "Books": 379.0,
                "Art": 678.0
            },
            "l03": {
                "Art": 474.0,
                "Vacation": 309.0,
                "Weather": 540.0,
                "Marriage": 843.0,
                "Yoga": 93.0,
                "Women": 62.0
            },
            "l01": {
                "Car": 312.0,
                "Television": 281.0,
                "Books": 315.0
            },
            "l11a": {
                "Apple": 444.0
            },
            "l109": {
                "Map": 1.0,
                "Shopping": 460.0,
                "Cycling": 35.0,
                "Weight": 532.0,
                "Car": 197.0,
                "Cooking": 142.0,
                "Baby": 49.0,
                "Love": 756.0,
                "Yoga": 124.0
            },
            "l108": {
                "Cycling": 23.0,
                "Weight": 882.0
            },
            "l121": {
                "Weather": 248.0,
                "Clothing": 703.0,
                "Weight": 346.0
            },
            "l120": {
                "Running": 227.0,
                "Books": 74.0
            },
            "l141": {
                "Shopping": 215.0,
                "Photography": 392.0,
                "Books": 70.0,
                "Health": 345.0,
                "Weather": 340.0
            },
            "l126": {
                "Baby": 83.0,
                "Vacation": 216.0
            },
            "l125": {
                "Crush": 388.0,
                "Food": 463.0,
                "Music": 139.0,
                "Weight": 424.0,
                "Home": 83.0
            },
            "l124": {
                "Love": 354.0,
                "Yoga": 800.0,
                "Health": 110.0,
                "Weight": 568.0,
                "Music": 465.0
            },
            "l101": {
                "Gaming": 37.0,
                "Marriage": 846.0,
                "Love": 574.0
            },
            "l103": {
                "Map": 262.0,
                "Photography": 491.0,
                "Weather": 76.0,
                "Art": 32.0,
                "Business": 261.0
            },
            "l102": {
                "Marriage": 162.0,
                "Photography": 192.0,
                "Clothing": 271.0,
                "Fishing": 9.0
            },
            "l105": {
                "Shopping": 132.0,
                "Weight": 196.0,
                "Vacation": 12.0,
                "Sports": 92.0,
                "Books": 550.0,
                "Bar": 302.0,
                "Women": 11.0
            },
            "l104": {
                "Android": 277.0,
                "Television": 249.0
            },
            "l107": {
                "Art": 393.0,
                "Food": 122.0,
                "Men": 766.0,
                "Books": 629.0,
                "Music": 200.0,
                "Baby": 770.0,
                "Shopping": 960.0
            },
            "l106": {
                "Cycling": 45.0
            }
        }
    };
    Social.topicImages = {
        "Android": Social.assetImgPath + "topic-icons/android.png",
        "Apple": Social.assetImgPath + "topic-icons/apple.png",
        "Art": Social.assetImgPath + "topic-icons/art.png",
        "Baby": Social.assetImgPath + "topic-icons/baby.png",
        "Bar": Social.assetImgPath + "topic-icons/bar.png",
        "Books": Social.assetImgPath + "topic-icons/books.png",
        "Business": Social.assetImgPath + "topic-icons/business.png",
        "Car": Social.assetImgPath + "topic-icons/car.png",
        "Cinema": Social.assetImgPath + "topic-icons/cinema.png",
        "Clothing": Social.assetImgPath + "topic-icons/clothing.png",
        "College": Social.assetImgPath + "topic-icons/college.png",
        "Cooking": Social.assetImgPath + "topic-icons/cooking.png",
        "Crush": Social.assetImgPath + "topic-icons/crush.png",
        "Cycling": Social.assetImgPath + "topic-icons/cycling.png",
        "Experiment": Social.assetImgPath + "topic-icons/experiment.png",
        "Family": Social.assetImgPath + "topic-icons/family.png",
        "Fishing": Social.assetImgPath + "topic-icons/fishing.png",
        "Food": Social.assetImgPath + "topic-icons/food.png",
        "Gaming": Social.assetImgPath + "topic-icons/gaming.png",
        "Health": Social.assetImgPath + "topic-icons/health.png",
        "Home": Social.assetImgPath + "topic-icons/home.png",
        "Internet": Social.assetImgPath + "topic-icons/internet.png",
        "Love": Social.assetImgPath + "topic-icons/love.png",
        "Map": Social.assetImgPath + "topic-icons/map.png",
        "Marriage": Social.assetImgPath + "topic-icons/marriage.png",
        "Men": Social.assetImgPath + "topic-icons/men.png",
        "Money": Social.assetImgPath + "topic-icons/money.png",
        "Music": Social.assetImgPath + "topic-icons/music.png",
        "Office": Social.assetImgPath + "topic-icons/office.png",
        "Photography": Social.assetImgPath + "topic-icons/photography.png",
        "Running": Social.assetImgPath + "topic-icons/running.png",
        "Shopping": Social.assetImgPath + "topic-icons/shopping.png",
        "Sports": Social.assetImgPath + "topic-icons/sports.png",
        "Television": Social.assetImgPath + "topic-icons/television.png",
        "Todo": Social.assetImgPath + "topic-icons/todo.png",
        "Vacation": Social.assetImgPath + "topic-icons/vacation.png",
        "Weather": Social.assetImgPath + "topic-icons/weather.png",
        "Weight": Social.assetImgPath + "topic-icons/weight.png",
        "Women": Social.assetImgPath + "topic-icons/women.png",
        "Yoga": Social.assetImgPath + "topic-icons/yoga.png"
    };
    Social.timeChartData = {
        dataLimitFrom: 1279408157,
        dataLimitTo: 1384253671,
        from: 1279408157,
        to: 1384253671,
        unit: "M",
        values: [
            [1280062860, 0.09307], [1282209412, 0.0769], [1284577510, 0.175],
            [1286988866, 0.2], [1289772560, 0.5], [1292221502, 0.3],
            [1295438521, 0.95], [1297635302, 1.1], [1300428904, 0.97],
            [1303393364, 4.14996], [1305747289, 9.4998], [1308021365, 31.9099],
            [1310688487, 16.74], [1313460176, 13.55], [1316163580, 8.7138],
            [1318917477, 5.25], [1329406625, 6.2], [1331712512, 5.45],
            [1334585184, 5.48], [1337032412, 5.19], [1339968960, 6.8],
            [1342565599, 9.7], [1345130283, 15.4], [1347658121, 12.68666],
            [1350406417, 13.0899], [1353158164, 12.6515], [1355482339, 13.90119],
            [1358620069, 21.43], [1360955055, 34.51541], [1363536829, 95.7],
            [1366088526, 266], [1368368125, 141.42236], [1371180511, 130.0],
            [1373758345, 111.652], [1376745412, 148.7], [1379147545, 148.90893],
            [1382024434, 233.4], [1383881325, 395]
        ]
    };
    function generatePieChartData() {
        // A function instead of a fixed value, because we need a clone of this data every time. This object will be modified later for every pieChart displayed.
        return [
            { id: "WhatsApp", value: 0, style: { fillColor: "#27ab19", icon: Social.assetImgPath + "icons/pie-whatsapp.png" } },
            { id: "Facebook", value: 0, style: { fillColor: "#3c599b", icon: Social.assetImgPath + "icons/pie-facebook.png" } },
            { id: "Email", value: 0, style: { fillColor: "#993", icon: Social.assetImgPath + "icons/pie-email.png" } },
            { id: "Twitter", value: 0, style: { fillColor: "#32ccfe", icon: Social.assetImgPath + "icons/pie-twitter.png" } },
            { id: "Google+", value: 0, style: { fillColor: "#de4b39", icon: Social.assetImgPath + "icons/pie-googleplus.png" } },
            { id: "Instagram", value: 0, style: { fillColor: "#975e4a", icon: Social.assetImgPath + "icons/pie-instagram.png" } },
            { id: "LinkedIn", value: 0, style: { fillColor: "#0274b3", icon: Social.assetImgPath + "icons/pie-linkedin-retina.png" } },
            { id: "Skype", value: 0, style: { fillColor: "#3e9dd7", icon: Social.assetImgPath + "icons/pie-skype.png" } }
        ];
    }
    Social.generatePieChartData = generatePieChartData;
};
/// <reference path="zoomcharts.d.ts" />
/// <reference path="socialData.ts" />
// Replace this with your own license

function SocialSetGraph(data, sourceId, clickCallBack) {
    var Social;
    Social = Social || {};
    var netChart;
    var overlayCharts = new Object(null);
    var pieChartDataCache = new Object(null);
    var auraMode = 0;
    setupNetChart(data);
    setupResizer();
	
    return netChart;
    function setupNetChart() {
        console.log(data);
        var container = document.getElementById('chartDiv');
        netChart = new NetChart({
            container: container,
            data: {
                preloaded: data
            }
                ,
            layout: {
                aspectRatio: false,
                nodeSpacing: 2
            },
            navigation: {
                mode: "focusnodes",
                initialNodes: [sourceId.toString()],
                focusNodeExpansionRadius: 2,
                numberOfFocusNodes: 2,
                focusNodeTailExpansionRadius: 0.5
            },
            advanced: {
                pointer: {
                    noClickOnDoubleClick: false
                }
            },
            events: {
                //onClick: netChartClick,
                onSelectionChange: function (event, args) {
                    //if (clickCallBack)
                        //clickCallBack(event, args);
                //    netChartSelectionChange(event, args);
                },
				onDoubleClick: function (event, args) {
					//debugger;
					event.preventDefault();
					if (clickCallBack)	clickCallBack(event, args);
                //onPositionChange: movePieChart
				}
			},
            interaction: {
                selection: {
                    allowMoveNodesOffscreen: false,
                    lockNodesOnMove: false
                },
                resizing: {
                    enabled: false
                },
                zooming: {
                    zoomExtent: [0.3, 6],
                    autoZoomExtent: [0.8, 3],
                    autoZoomSize: 0.9,
                    wheel: window.top === window,
                    initialAutoZoom: 'false'
                }
            },
            nodeMenu: {
                enabled: false
            },
            linkMenu: {
                enabled: false
            },
            style: {
                fadeTime: 200,
                node: {
                    imageCropping: true
                },
                nodeRadiusExtent: [20, 30],
                nodeAutoScaling: null,
                nodeHovered: {
                    fillColor: "white",
                    shadowColor: "#419a00",
                    shadowOffsetY: 2
                },
                linkHovered: {
                    fillColor: "#419a00",
                    shadowColor: "#419a00"
                },
                nodeLabel: {
                    padding: 1,
                    borderRadius: 2,
                    textStyle: {
                        font: "10px Roboto",
                        fillColor: "white"
                    },
                    backgroundStyle: {
                        fillColor: "rgba(0,153,204,0.8)"
                    }
                },
                linkLabel: {
                    padding: 1,
                    borderRadius: 1,
                    textStyle: {
                        font: "4px Roboto", fillColor: "white"
                    },
                    backgroundStyle: {
                        fillColor: "rgba(0,0,0,0.7)", lineColor: "transparent"
                    }
                },
                nodeStyleFunction: nodeStyle,
                linkStyleFunction: linkStyle,
                selection: {
                    sizeConstant: 0,
                    sizeProportional: 0
                }
            },
            toolbar: {
                enabled: true,
                extraItems: [
                    {
                        label: "Aura",
                        showLabel: true,
                        side: "bottom",
                        align: "left",
                        onClick: function (event, chart) {
                            auraMode = (auraMode + 1) % 3;
                            switch (auraMode) {
                                case 0:
                                    chart.updateSettings({ auras: { enabled: false } });
                                    break;
                                case 1:
                                    chart.updateSettings({ auras: { enabled: true, overlap: true } });
                                    break;
                                case 2:
                                    chart.updateSettings({ auras: { enabled: true, overlap: false } });
                                    break;
                            }
                        }
                    }
                ]
            },
            auras: {
                enabled: false,
                cellSize: 8,
                overlap: false
            }
        });
    }
    function setupResizer() {
        function updateHeight() {
            netChart.updateSettings({ area: { height: Math.max(100, window.innerHeight) } });
        }
        ;
        window.addEventListener('resize', updateHeight);
        window.addEventListener('orientationchange', updateHeight);
        updateHeight();
    }
    function nodeStyle(node) {
        node.radius = node.focused ? 30 : Math.max(node.relevance * 10, 20);
        //node.image = Social.assetImgPath + "faces/" + node.id + ".png";
        node.image = node.data.extra.image;
        node.aura = (parseInt(node.id.substring(2), 10) % 4).toString();
        node.fillColor = null;
        var label = node.data.extra.name;
        if (node.selected) {
            node.label = "";
            node.items = [{
                text: label,
                px: 0, py: 1, x: 0, y: -20,
                textStyle: {
                    fillColor: "black"
                },
                backgroundStyle: {
                    fillColor: "rgba(255,255,255,0.7)"
                }
            }];
        }
        else {
            node.label = label;
            node.items = [];
        }
    }
    function linkStyle(link) {
        if (link.hovered) {
            link.radius = 2;
            link.label = link.data.extra.type;
        }
        else {
            link.radius = 1;
        }
        if ((link.from.focused && link.from.hovered) || (link.to.focused && link.to.hovered)) {
            link.fillColor = "green";
        }
        else if (link.data.extra.type === "colleague") {
            link.fillColor = "#fa0";
        }
        else {
            link.fillColor = "#09c";
        }
    }
    function netChartClick(event, args) {
        if (!event.ctrlKey && !event.shiftKey && args.clickNode) {
            netChart.addFocusNode(args.clickNode);
            event.preventDefault();
        }
    }
    function netChartSelectionChange(event, args) {
        var oldCharts = overlayCharts;
        overlayCharts = new Object(null);
        for (var i = 0; i < args.selection.length; i++) {
            var node = args.selection[i];
            if (oldCharts[node.id]) {
                overlayCharts[node.id] = oldCharts[node.id];
                delete oldCharts[node.id];
            }
            else {
                var newChart = createPieChart(node);
                overlayCharts[node.id] = {
                    node: node,
                    pieChart: newChart.pieChart,
                    pieChartSettings: newChart.pieChartSettings,
                    timeChart: null,
                    timeChartSettings: null
                };
            }
        }
        var keys = Object.keys(oldCharts);
        for (var i = 0; i < keys.length; i++) {
            var item = oldCharts[keys[i]];
            if (item.timeChart)
                item.timeChart.remove();
            item.pieChart.remove();
            item.pieChart = null;
            item.timeChart = null;
        }
    }
    function createPieChart(node) {
        var dimensions = getPieChartDimensions(node);
        var settings = {
            parentChart: netChart,
            area: dimensions.area,
            pie: dimensions.pie,
            data: [{ preloaded: { subvalues: getPieChartData(node.id) } }],
            labels: {
                enabled: false
            },
            events: {
                onClick: function (e, a) { return pieChartClickFunction(e, a, node.id); }
            },
            icons: {
                autohideWhenTooSmall: false,
                sizeExtent: [0, 64]
            },
            credits: {
                enabled: false
            }
        };
        settings.pie.adaptiveRadius = false;
        settings.pie.styleFunction = pieStyleFunction;
        return { pieChart: new PieChart(settings), pieChartSettings: settings };
    }
    function movePieChart() {
        var keys = Object.keys(overlayCharts);
        for (var i = 0; i < keys.length; i++) {
            var item = overlayCharts[keys[i]];
            var newPieChartSettings = getPieChartDimensions(item.node);
            if (item.pieChartSettings.area.left !== newPieChartSettings.area.left
                || item.pieChartSettings.area.top !== newPieChartSettings.area.top
                || item.pieChartSettings.pie.radius !== newPieChartSettings.pie.radius) {
                item.pieChartSettings = newPieChartSettings;
                item.pieChart.updateSettings(newPieChartSettings);
            }
            if (item.timeChart) {
                var newTimeChartArea = getTimechartDimensions(item.node);
                if (item.timeChartSettings.area.left !== newTimeChartArea.left
                    || item.timeChartSettings.area.top !== newTimeChartArea.top
                    || item.timeChartSettings.area.width !== newTimeChartArea.width
                    || item.timeChartSettings.area.height !== newTimeChartArea.height) {
                    var newTimeChartSettings = { area: newTimeChartArea };
                    item.timeChart.updateSettings(newTimeChartSettings);
                    item.timeChartSettings = newTimeChartSettings;
                }
            }
        }
    }
    function pieChartClickFunction(event, args, nodeId) {
        var item = overlayCharts[nodeId];
        if (args.slice && args.slice.expandable == false && args.slice.selected == false) {
            if (item.timeChart)
                item.timeChart.remove();
            var newChart = createTimeChart(item.node, args.slice);
            ;
            item.timeChart = newChart.timeChart;
            item.timeChartSettings = newChart.timeChartSettings;
        }
        if ((args.slice == null || args.slice.selected == true) && item.timeChart) {
            item.timeChart.remove();
            item.timeChart = null;
        }
    }
    function pieStyleFunction(pie) {
        if (pie.parentSlice) {
            pie.sliceColors = [pie.parentSlice.fillColor];
            pie.colorDistribution = "gradient";
        }
    }
    function getPieChartData(nodeId) {
        if (!pieChartDataCache[nodeId]) {
            var topics = Social.graphTopics.nodes[nodeId];
            var pieChartData = Social.generatePieChartData();
            for (var i in pieChartData) {
                if (nodeId == Social.netChartDefaultValue && pieChartData[i].id == 'Facebook') {
                    // Facebook boost for Julia
                    pieChartData[i].value = 250;
                }
                else {
                    pieChartData[i].value = Math.floor(Math.random() * (Social.pieChartMaxValue - Social.pieChartMinValue + 1)) + Social.pieChartMinValue;
                }
                var subvalues = [];
                for (var topic in topics) {
                    subvalues.push({
                        name: topic,
                        value: topics[topic],
                        style: {
                            icon: Social.topicImages[topic],
                            label: { text: topic + " (" + topics[topic] + ")" }
                        }
                    });
                }
                pieChartData[i].subvalues = subvalues;
            }
            pieChartDataCache[nodeId] = pieChartData;
        }
        return pieChartDataCache[nodeId];
    }
    function getPieChartDimensions(node) {
        var dimensions = netChart.getNodeDimensions(node);
        return {
            area: {
                left: dimensions.x - dimensions.radius * 5,
                top: dimensions.y - dimensions.radius * 5,
                width: dimensions.radius * 10,
                height: dimensions.radius * 10
            },
            pie: {
                radius: dimensions.radius + (dimensions.radius / 2),
                innerRadius: dimensions.radius,
                centerMargin: Math.round(dimensions.radius * 0.7)
            }
        };
    }
    function createTimeChart(node, slice) {
        var settings = {
            parentChart: netChart,
            area: getTimechartDimensions(node),
            credits: {
                enabled: false
            },
            data: [{
                timestampInSeconds: true,
                units: ["M"],
                preloaded: Social.timeChartData
            }],
            toolbar: {
                enabled: false
            },
            interaction: {
                resizing: {
                    enabled: false
                }
            },
            valueAxisDefault: {
                side: "right",
                position: "outside"
            },
            series: [{
                id: "timechart",
                data: { index: 1 },
                style: { fillColor: slice.fillColor }
            }]
        };
        settings.area.style = { fillColor: "rgba(243,243,243,1)" };
        return { timeChart: new TimeChart(settings), timeChartSettings: settings };
    }
    function getTimechartDimensions(node) {
        var nodeDimensions = netChart.getNodeDimensions(node);
        var width = Math.min(200, window.innerWidth - 100);
        var height = Math.min(100, window.innerHeight - 100);
        var top = nodeDimensions.y - height + 20;
        var left = nodeDimensions.x + nodeDimensions.radius + 100;
        if (left + width >= window.innerWidth) {
            left = nodeDimensions.x - nodeDimensions.radius - 150 - width;
        }
        return {
            width: 800,
            height: 400,
            top: 0,
            left: 0,
        };
    }
};
//Social.RunDemo();
//Social.dummy();
//# sourceMappingURL=social.js.map